"""Define the main function's argument parser and validation functions."""
import argparse
import os

from src.ops import POOLING


def existing_dir(dirname):
  """Verify that a directory exists."""
  if not os.path.isdir(dirname):
    raise ValueError(f"Directory with name {dirname} doesn't exist.")
  return dirname


def valid_assets_dir(assets_dir):
  """Validate the assets directory."""
  existing_dir(assets_dir)
  expected_subdirs = ["plates", "splits"]
  assets_subdirs = os.listdir(assets_dir)
  if not all(a_dir in assets_subdirs for a_dir in expected_subdirs):
    raise ValueError(
        f"Assets directory should contain at least {expected_subdirs} "
        "subdirectories")
  return assets_dir


def valid_split(split_id):
  """Validate the split_id argument."""
  split_id = int(split_id)
  if not 1 <= split_id <= 3:
    raise ValueError("Split ID should be an integer in the range [1, 3].")
  return split_id


parser = argparse.ArgumentParser(description="Process the project directories.")
parser.add_argument(
    "-a",
    "--assets",
    default="assets",
    dest="assets_dir",
    type=valid_assets_dir,
    help="The directory where the project assets are placed.")
parser.add_argument(
    "-o",
    "--output",
    default="bin",
    dest="output_dir",
    help="The directory where the project outputs are placed.")
parser.add_argument(
    "--flim",
    action="store_true",
    help="Whether to use FLIM to compute kernel bank.")
parser.add_argument(
    "--patch_strategy",
    default="sliding",
    choices=["superpixel", "sliding"],
    help="Strategy used to extract patches from the original plate images. "
    "Superpixel uses the DISF algorithm to compute superpixels and extracts "
    "patches centered in each superpixel. Sliding strategy uses the plate "
    "info from masks to extract patches in a sliding window fashion.")
parser.add_argument(
    "--batch_norm",
    action="store_true",
    help="Whether to apply batch normalization to the dataset, "
    "subtracting the mean and dividing by the standard "
    "deviation between all samples in the training set.")
parser.add_argument(
    "--kernel_norm",
    action="store_true",
    help="Whether to unit normalize the kernels. If false, "
    "kernel weights are drawn from a uniform distribution "
    "in the range [0, 1].")
parser.add_argument(
    "--overwrite",
    action="store_true",
    help="Whether to overwritten existing preprocessed data.")
parser.add_argument(
    "--plate_threshold",
    default=0.5,
    type=float,
    help="The percentage of the plate mask that needs to be within the patch "
    "in order to consider it a plate.")
parser.add_argument(
    "--classifier",
    default="mlp",
    choices=["svm", "mlp"],
    dest="classifier_type",
    help="Type of classifier used for binary classification after extracting "
    "features.")
parser.add_argument(
    "--pooling",
    default="relu",
    choices=POOLING.keys(),
    help="The pooling operation used after the relu activation and before "
    "feeding the classifier.")
parser.add_argument(
    "--bands",
    default=16,
    type=int,
    help="Number of bands of the kernel bank used to extract features")
parser.add_argument(
    "--kernel_size",
    default=3,
    type=int,
    help="The kernel size used to define the shape of the kernel bank.")
parser.add_argument(
    "--seed",
    default=42,
    type=int,
    help="The seed used for the random ops in the program.")

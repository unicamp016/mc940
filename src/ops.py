# pylint: disable=invalid-name
"""Multiband image operations."""
import numpy as np
import torch
from scipy import signal
from skimage.measure import block_reduce
from sklearn.neural_network import MLPClassifier
from sklearn.neural_network._base import ACTIVATIONS
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from torch.nn.functional import conv2d
from tqdm import tqdm

from src.consts import DEVICE
from src.util import (Batch, batch_labels, rescale, save_batch,
                      weighted_region_coords)


class Classifer:
  """Performs the downstream task of classification from extracted features."""

  def __init__(self, classifier_type):
    """Initialize custom classifier.

    Args:
        classifier_type: The type of classifier we wish to use. One of {'svm',
          'mlp'}.
    """
    if classifier_type == "svm":
      self._model = SVC(
          C=1e2, kernel="linear", class_weight="balanced", verbose=True)
    elif classifier_type == "mlp":
      self._model = MLPClassifier(
          hidden_layer_sizes=[512, 256],
          activation="relu",
          verbose=True,
          max_iter=50)
    else:
      raise NotImplementedError(f"Classifier {classifier_type} is not "
                                "supported.")
    # self._clf = make_pipeline(StandardScaler(), self._model)
    self._clf = make_pipeline(self._model)

  def __len__(self):
    """Return the number of layers for the MLP classifier."""
    assert isinstance(
        self._model, MLPClassifier
    ), "Classifier length is only supported for MLPClassifier instances."
    return len(self._model.coefs_)

  def fit(self, X, y):
    """Fit the model with the given inputs and labels.

    Args:
        X: Input features to train on. Expects numpy array with shape (N, D).
        y: Class labels for each sample. Expects shape (N,)
    """
    return self._clf.fit(X, y)

  def predict(self, X):
    """Predict the class for the given inputs.

    Args:
        X: Input features we wish to predict a class for. Expects a numpy
          array of the form (N, D).
    """
    return self._clf.predict(X)

  def predict_prob(self, X):
    """Predict the class probabilities for the given inputs.

    Args:
        X: Input features we wish to predict a class for. Expects a numpy
          array of the form (N, D).
    """
    return self._clf.predict_proba(X)

  def activations(self, X, layer):
    """Get the activations for requested layer.

    Args:
      X: Input features we wish to get intermediate activations for.
      layer: Integer with the layer number we wish to retrieve (index starting
        at 1)
    """
    assert isinstance(
        self._model, MLPClassifier
    ), "Intermediate activations are only supported for MLPClassifier."
    assert 0 < layer <= len(
        self._model.coefs_), f"Invalid layer number {layer} requested."

    if self._clf.steps[0][0] == "standardscaler":
      scaler = self._clf.steps[0][1]
      X = scaler.transform(X)
    acts = X
    for l in range(layer):
      acts = np.matmul(acts, self._model.coefs_[l]) + self._model.intercepts_[l]
      ACTIVATIONS[self._model.activation](acts)  # inplace
    return acts


def conv_fast(batch, kernel_bank):
  """Fast implementation of batch convolution using torch layer."""
  k_h, k_w = kernel_bank.shape[1:3]
  torch_batch = torch.from_numpy(batch)
  torch_kernel_bank = torch.from_numpy(kernel_bank)
  torch_batch = torch_batch.permute(0, 3, 1, 2).float().to(DEVICE)
  torch_kernel_bank = torch_kernel_bank.permute(0, 3, 1, 2).float().to(DEVICE)
  torch_conv = conv2d(
      torch_batch, torch_kernel_bank, padding=(k_h // 2, k_w // 2))
  conv = torch_conv.permute(0, 2, 3, 1).detach().numpy()
  return Batch.from_batch(conv, batch)


def convolve_kernel_bank(batch, kernel_bank):
  """Convolve a batch of images with a kernel bank.

  Args:
    batch: A 4-D numpy array representing a batch of images. Batch shape is
      (batch_size, height, width, channels)
    kernel_bank: A 4-D numpy array representing a kernel bank. The kernel bank
      shape is:
      (bands, kernel_height, kernel_width, channels)

  Returns:
    A 4-D numpy array with the resulting batch of images after convolving the
    kernel bank through it. Output shape is (batch_size, height, width, bands).
  """
  assert batch.shape[-1] == kernel_bank.shape[-1]
  chans = batch.shape[-1]
  conv_batch = []
  for img in batch:
    conv_bands = []
    for kernel_band in kernel_bank:
      # Convolution needs to be in mode same in order to maintain height and
      # width dimensions. However, we end up with extra operations that also
      # pad the channel dimensions. Therefore, we only keep the center-most
      # band from the conv result.
      conv = signal.convolve(img, kernel_band, mode="same")
      conv_band = conv[..., chans // 2]
      conv_bands.append(conv_band)
    conv_batch.append(np.stack(conv_bands, axis=-1))
  conv_batch_np = np.stack(conv_batch)
  conv_batch = Batch.from_batch(conv_batch_np, batch)
  return conv_batch


def flatten(batch):
  """Flatten the feature dimensions to a single vector."""
  return batch.reshape(batch.shape[0], -1)


def relu(batch):
  """Apply the ReLU activation to the input batch.

  Args:
    batch: A numpy array representing a batch of images.

  Returns:
    The rectified batch with only non-negative activations.
  """
  return np.maximum(batch, 0)


def strides(batch, kernel_shape):
  """Return views for the strides along the height and width of each image.

  Args:
    batch: A 4-D numpy array with shape (batch_size, height, width, channels).
    kernel_shape: A 2-tuple with the shape of the strides in the form
      (kernel_height, kernel_width).

  Returns:
    The strides for each pixel in the images of the batch with shape
      (batch_size, height, width, kernel_height, kernel_width, channels).
  """
  b, h, w, c = batch.shape
  k_h, k_w = kernel_shape
  padded_batch = pad_batch(batch, kernel_shape)
  shape = (b, h, w, k_h, k_w, c)
  b_str, h_str, w_str, c_str = padded_batch.strides
  strs = (b_str, h_str, w_str, h_str, w_str, c_str)
  return np.lib.stride_tricks.as_strided(
      padded_batch, shape=shape, strides=strs)


def maxpool(batch, kernel_shape):
  """Apply max pooling to entire batch."""
  batch_strides = strides(batch, kernel_shape)
  return Batch.from_batch(np.max(batch_strides, axis=(3, 4)), batch)


def maxpool_reduce(batch, strides):
  """Apply max pooling with `strides` to reduce input dimensionality."""
  return block_reduce(batch, (1, strides[0], strides[1], 1), np.max)


def avgpool(batch, kernel_shape):
  """Apply average pooling to the entire batch."""
  batch_strides = strides(batch, kernel_shape)
  return Batch.from_batch(np.mean(batch_strides, axis=(3, 4)), batch)


def integral(batch):
  """Compute the integral image for each filter."""
  return np.cumsum(np.cumsum(batch, axis=1), axis=2)


def pad_batch(batch, kernel_size, mode="edge"):
  """Pad the batch in order to stride a kernel with `kernel_size`.

  For every pixel in the batch, we should be able to apply a kernel of
  dimensions (kernel_height, kernel_width). Padding is done using edge mode and
  always prioritizing left and top padding. That is, if the kernel has an even
  dimension, the extra row or column of padding is added to the top or left of
  the image, respectively.
  """
  k_h, k_w = kernel_size
  pad_top = k_h // 2
  pad_bot = -(pad_top) + k_h - 1
  pad_left = k_w // 2
  pad_right = -(pad_left) + k_w - 1
  return np.pad(
      batch,
      pad_width=[(0, 0), (pad_top, pad_bot), (pad_left, pad_right), (0, 0)],
      mode=mode)


def integral_value(padded_integral_batch, shape, region):
  """Compute the integral value for a bounding box of the integral image.

  Args:
    padded_integral_batch: A padded integral batch.
    shape: The output shape of the integral values batch.
    region: A list of lists with two tuples in the form:
      [(o_y, o_x), (h_y, w_x)], where (o_y, o_x) is the coordinate for the
      origin of the region and (h_y, w_x) is the coordinate for the diagonal
      opposite to the origin.

  Returns:
    A batch with the original shape of the integral image with the computed
    integral values of the region for each pixel.
  """
  # Additional padding to ease computation of integral values in points out of
  # the boundaries of the image.
  padded_integral_batch = np.pad(
      padded_integral_batch,
      pad_width=[(0, 0), (1, 0), (1, 0), (0, 0)],
      mode="constant",
      constant_values=0)
  p1_y, p1_x, p4_y, p4_x = [], [], [], []
  for coords in region:
    p1_y.append(coords[0][0])
    p1_x.append(coords[0][1])
    p4_y.append(coords[1][0])
    p4_x.append(coords[1][1])
  p1_y, p1_x, p4_y, p4_x = tuple(map(np.array, [p1_y, p1_x, p4_y, p4_x]))
  # This ensures that we can compute the integral value of a region with only
  # four arithmetic operations.
  p4_y += 1
  p4_x += 1
  p2_y, p2_x = p1_y, p4_x
  p3_y, p3_x = p4_y, p1_x

  # Compute the integral values
  i1 = padded_integral_batch[:, p1_y, p1_x, :]
  i2 = padded_integral_batch[:, p2_y, p2_x, :]
  i3 = padded_integral_batch[:, p3_y, p3_x, :]
  i4 = padded_integral_batch[:, p4_y, p4_x, :]
  integral_values = i1 + i4 - i2 - i3
  return np.reshape(integral_values, newshape=shape)


def haar_like_feature(batch, window_size, feat_type="type-3-y"):
  """Compute the haar like feature for every pixel in the batch."""
  h, w = batch.shape[1:3]
  padded_batch = pad_batch(batch, window_size, mode="constant")
  integral_batch = integral(padded_batch)
  regions, weights = weighted_region_coords((h, w), window_size, feat_type)
  output_batch = Batch.from_batch(
      np.zeros(batch.shape, dtype=np.float32), batch)
  for r, w in zip(regions, weights):
    output_batch += w * integral_value(integral_batch, batch.shape, r)
  return output_batch


class FeatureExtractor:

  def __init__(self,
               train_dataset,
               flim,
               batch_norm,
               pooling,
               kernel_bank=None,
               model=None):
    self.train_dataset = train_dataset
    self.flim = flim
    self.batch_norm = batch_norm
    self.pooling = pooling
    self.kernel_bank = kernel_bank
    self.model = model

  def forward_pass(self, dataset):
    """Extract features from the given dataset.

    Return the output of the feature extractor applied to the dataset and the
    respective labels of each sample.
    """
    outputs, labels, filenames = [], [], []
    for batch in tqdm(dataset, desc="Processing data", position=0, leave=True):
      if self.batch_norm:
        batch = (batch - self.train_dataset.mean) / self.train_dataset.std

      if self.flim:
        torch_batch = torch.from_numpy(batch)
        torch_batch = torch_batch.permute(0, 3, 1, 2).float().to(DEVICE)
        conv = self.model.forward(torch_batch)
        conv = conv.permute(0, 2, 3, 1).detach().numpy()
        conv = Batch.from_batch(conv, batch)
      else:
        conv = conv_fast(batch, self.kernel_bank)
        conv = relu(conv)

      # TODO: possibly save images, don't forget to rescale and change type
      pool = POOLING[self.pooling](conv)

      save_batch(rescale(pool).astype(np.uint8), "pool")

      # Final pooling operation to reduce dimensionality for classifer
      # TODO: possibly save images, don't forget to rescale
      # TODO: apply t-sne to visualize classes
      output = maxpool_reduce(pool, strides=(2, 2))

      outputs.append(output)
      labels.append(batch_labels(batch))
      filenames.extend(batch.filenames)
    labels = np.concatenate(labels, axis=0)
    outputs = np.concatenate(outputs, axis=0)
    outputs = flatten(outputs)
    return outputs, labels, filenames


POOLING = {
    "relu":
        lambda relu: relu,
    "maxpool3x3":
        lambda relu: maxpool(relu, kernel_shape=(3, 3)),
    "maxpool7x7":
        lambda relu: maxpool(relu, kernel_shape=(7, 7)),
    "avgpool3x3":
        lambda relu: avgpool(relu, kernel_shape=(3, 3)),
    "avgpool7x7":
        lambda relu: avgpool(relu, kernel_shape=(7, 7)),
    "integral":
        integral,
    "haar3y3x3":
        lambda relu_out: relu(
            haar_like_feature(
                relu_out, window_size=(3, 3), feat_type="type-3-y")),
    "haar3y9x9":
        lambda relu_out: relu(
            haar_like_feature(
                relu_out, window_size=(9, 9), feat_type="type-3-y")),
    "haar3x3x3":
        lambda relu_out: relu(
            haar_like_feature(
                relu_out, window_size=(3, 3), feat_type="type-3-x")),
    "haar3x9x9":
        lambda relu_out: relu(
            haar_like_feature(
                relu_out, window_size=(9, 9), feat_type="type-3-x")),
    "haarplate35x70":
        lambda relu_out: relu(
            haar_like_feature(
                relu_out, window_size=(35, 70), feat_type="plate")),
    "haarplate35x35":
        lambda relu_out: relu(
            haar_like_feature(
                relu_out, window_size=(35, 35), feat_type="plate")),
}

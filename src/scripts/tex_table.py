import argparse
import glob
import os
import re
import shutil

import src.util as util

parser = argparse.ArgumentParser()

parser.add_argument(
    "-f",
    "--metric_file",
    type=argparse.FileType("r"),
    nargs="+",
    help="One or more metric file we wish to render as a latex table.")
parser.add_argument(
    "-n",
    "--metric_type",
    type=util.Metric,
    choices=list(util.Metric),
    nargs="+",
    help="The metric names we wish to add a column for in the latex table.")
parser.add_argument(
    "-i",
    "--image_dir",
    type=str,
    nargs="+",
    help="The directory names with the images of the kernels output for metrics."
)
parser.add_argument(
    "--top", type=int, help="The number of top kernels to add to the table.")
parser.add_argument(
    "-o",
    "--output_dir",
    type=str,
    help="The output directory to write the latex artifacts to.")


def latex_kernel_id(kernel_id, kernel_dims):
  if kernel_id == 0:
    latex_id = "$G_x$"
  elif kernel_id == 1:
    latex_id = "$G_y$"
  else:
    latex_id = f"$k_{{{kernel_id}({kernel_dims[0]}\\times{kernel_dims[1]})}}$"
  return latex_id


def merge_two_columns(column1, column2):
  merged_columns = ""
  column1_lines = column1.strip().split("\n")
  column2_lines = column2.strip().split("\n")
  for l1, l2 in zip(column1_lines, column2_lines):
    merged_columns += f"{l1} & {l2}\n"
  return merged_columns


def merge_columns(columns):
  assert len(columns) >= 2
  table = merge_two_columns(columns[0], columns[1])
  for c in columns[2:]:
    table = merge_two_columns(table, c)
  return table


def save_table(metrics, output_dir):
  metric_columns = {}
  for opname in metrics:
    for metric_name, kernels in metrics[opname].items():
      metric_column = ""
      for kernel_id, kernel_dims, _, metric_value in kernels:
        latex_id = latex_kernel_id(kernel_id, kernel_dims)
        metric_column += f"{latex_id: <18} & {metric_value:.2f}\n"
      metric_columns[f"{opname}-{metric_name}"] = metric_column
  sorted_metric_columns = [metric_columns[k] for k in sorted(metric_columns)]
  merged_columns = merge_columns(sorted_metric_columns)

  metric_table = "%"
  for k in sorted(metric_columns):
    metric_table += f" {k}"
  metric_table += "\n"
  for row in merged_columns.strip().split("\n"):
    metric_table += f"{row} \\\\\n"
  with open(os.path.join(output_dir, "table.tex"), "w") as f:
    f.write(metric_table)


def save_figure(metrics, opname, output_dir):
  for metric_name, kernels in metrics.items():
    latex_figure = "\\begin{figure}\n  \\centering\n"
    for i, (kernel_id, kernel_dims, fname, _) in enumerate(kernels):
      # Copy image to output dir
      basename = os.path.basename(fname)
      pardir = os.path.basename(os.path.dirname(fname))
      figdir = os.path.join(output_dir, pardir)
      os.makedirs(figdir, exist_ok=True)
      shutil.copyfile(fname, os.path.join(figdir, basename))

      # Add the subfigure directive to latex
      figname = os.path.join(pardir, basename)
      latex_id = latex_kernel_id(kernel_id, kernel_dims)
      latex_figure += "  \\begin{subfigure}{0.25\\textwidth}\n"
      latex_figure += f"    \\includegraphics[width=\\textwidth]{{images/{figname}}}\n"
      latex_figure += f"    \\caption{{{latex_id}}}\n"
      latex_figure += f"    \\label{{fig:{opname}-{metric_name}-{i}}}\n"
      latex_figure += "  \\end{subfigure}\n"
    latex_figure += "  \\caption{}\n"
    latex_figure += f"  \\label{{fig:{opname}-{metric_name}}}\n"
    latex_figure += "\\end{figure}\n"
    with open(
        os.path.join(output_dir, opname, f"{opname}-{metric_name}.tex"),
        "w") as f:
      f.write(latex_figure)


def main(metric_file, image_dir, metric_type, top, output_dir):
  metric_names = [m.name for m in metric_type]
  metrics_dict = {}
  for mfile, imgdir in zip(metric_file, image_dir):
    opname = os.path.basename(imgdir)
    metrics_dict[opname] = {}
    line = mfile.readline()
    while line:
      cur_mname = line.split(":")[0]
      if cur_mname in metric_names:
        metrics_dict[opname][cur_mname] = []
        for _ in range(top):
          tokens = mfile.readline().split()
          kernel_id = int(tokens[2])
          if kernel_id <= 1:
            base_idx = 4
          else:
            base_idx = 3
          kernel_dims = (int(re.findall("[0-9]+", tokens[base_idx])[0]),
                         int(re.findall("[0-9]+", tokens[base_idx + 1])[0]))
          kernel_fn = sorted(
              glob.glob(os.path.join(imgdir, f"1_*_{kernel_id:03d}.png")))[0]
          metric_value = float(tokens[-1])
          metrics_dict[opname][cur_mname].append(
              (kernel_id, kernel_dims, kernel_fn, metric_value))
      line = mfile.readline()
  os.makedirs(output_dir, exist_ok=True)
  for opname in metrics_dict:
    save_figure(metrics_dict[opname], opname, output_dir)
  save_table(metrics_dict, output_dir)


if __name__ == "__main__":
  args = parser.parse_args()
  main(**vars(args))

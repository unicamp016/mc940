import os

import cv2
import matplotlib
import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np

matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem":
        "pdflatex",
    "font.family":
        "serif",
    "text.usetex":
        True,
    "pgf.rcfonts":
        False,
    "pgf.preamble":
        r" ".join([
            r"\usepackage[utf8x]{inputenc}",
            r"\usepackage[T1]{fontenc}",
            r"\usepackage{amsmath}",
        ])
})

jet = cm.get_cmap("jet")
gray = np.arange(256)
rgb = jet(gray, bytes=True)

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(gray, rgb[:, 0], color="red")
ax.plot(gray, rgb[:, 1], color="green")
ax.plot(gray, rgb[:, 2], color="blue")
fig.colorbar(cm.ScalarMappable(norm=colors.Normalize(0, 255), cmap=jet), ax=ax)

fig.savefig("cmap_plot.pdf")

plates_dir = "assets/plates"
fns = ["orig_0003.png", "orig_0007.png", "orig_0010.png"]
fns = [os.path.join(plates_dir, fn) for fn in fns]
for fn in fns:
  gray = cv2.imread(fn, cv2.IMREAD_GRAYSCALE)
  rgba = jet(gray, bytes=True)
  bgr = rgba[..., 2::-1]
  cv2.imwrite(fn.replace("orig", "rgb"), bgr)

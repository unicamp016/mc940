import json
import os

import fire
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from src.data import Dataset, DatasetSplit, SplitType
from src.util import batch_labels
from tqdm import tqdm

# matplotlib.use("pgf")
# matplotlib.rcParams.update({
# "pgf.texsystem":
# "pdflatex",
# "font.family":
# "serif",
# "text.usetex":
# True,
# "pgf.rcfonts":
# False,
# "pgf.preamble":
# r" ".join([
# r"\usepackage[utf8x]{inputenc}",
# r"\usepackage[T1]{fontenc}",
# r"\usepackage{amsmath}",
# ])
# })

BACKGROUND = 0
FOREGROUND = 1


class PointSelector:

  def __init__(self, fig, max_coords=4):
    self.coords = []
    self.max_coords = max_coords
    self.fig = fig
    self.cid = self.fig.canvas.mpl_connect("button_press_event", self)

  def __call__(self, event):
    """Handle click event for on figure and store click coordinates."""
    x, y = event.xdata, event.ydata
    self.coords.append((x, y))
    print(f"Registred a click on position ({x}, {y})")
    if len(self.coords) >= self.max_coords:
      print("Collected all coords, closing.")
      self.fig.canvas.mpl_disconnect(self.cid)
      plt.close(self.fig)


def find_nearest(array, value):
  """Return the index in `array` of the element closest to value (L1-dist)."""
  array = np.asarray(array)
  idx = (np.linalg.norm(array - value, axis=1)).argmin()
  return idx


def main(exp_dir):
  config_fn = os.path.join(exp_dir, "config.json")
  with open(config_fn, "rb") as cf:
    config = json.load(cf)

  for split in range(1, 4):
    train_split = DatasetSplit(
        split, SplitType.train, config["assets_dir"], exp_dir, overwrite=False)
    train_dataset = Dataset(train_split)
    xs, ys, files = [], [], []
    for batch in tqdm(train_dataset):
      batch_size = batch.shape[0]
      xs.append(batch.reshape(batch_size, -1))
      ys.append(batch_labels(batch))
      files.extend(batch.filenames)
    X = np.concatenate(xs, axis=0)
    X = (X - np.mean(X, axis=0)) / np.std(X, axis=0)
    ys = np.concatenate(ys, axis=0)
    plate = ys == FOREGROUND
    backg = ys == BACKGROUND

    # First reduce dimensionality before feeding to t-SNE
    # pca = PCA(n_components=50)
    # X_pca = pca.fit_transform(X)

    pca = PCA(n_components=2)
    Y = pca.fit_transform(X)
    tsne_dir = os.path.join(exp_dir, "tsne")
    os.makedirs(tsne_dir, exist_ok=True)
    for perplexity in [5, 25, 50, 100]:
      print(f"Running TSNE for perplexity={perplexity}")
      # tsne = TSNE(n_components=2, verbose=1, perplexity=perplexity)
      # Y = tsne.fit_transform(X_pca)
      fig, ax = plt.subplots()
      plt.axis('off')
      ax.scatter(
          Y[backg, 0], Y[backg, 1], s=1, c="r", alpha=0.5, label="background")
      ax.scatter(Y[plate, 0], Y[plate, 1], s=1, c="g", alpha=0.7, label="plate")
      ax.get_xaxis().set_visible(False)
      ax.get_yaxis().set_visible(False)
      # pselect = PointSelector(fig)
      # plt.show()
      # S = []
      # click_fnames_fn = os.path.join(
      # tsne_dir, f"split-{split}-perp-{perplexity}-fnames.txt")
      # with open(click_fnames_fn, "w") as f:
      # for click_coord in pselect.coords:
      # file_idx = find_nearest(Y, click_coord)
      # S.append(Y[file_idx])
      # f.write(f"{files[file_idx]}\n")
      # S = np.array(S)
      # ax.scatter(S[:, 0], S[:, 1], c="orange", alpha=1.0, label="selected")
      # ax.legend()
      fig.savefig(
          os.path.join(tsne_dir, f"split-{split}-pca.pdf"),
          bbox_inches="tight",
          pad_inches=0)
      break


if __name__ == "__main__":
  fire.Fire(main)

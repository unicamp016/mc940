import glob
import os

import cv2
import fire
import matplotlib.pyplot as plt
import numpy as np
from src.util import load_images_and_markers


def vis_markers(name, assets_dir="assets", output_dir="bin"):
  """Visualize the drawn markers over the original images."""
  markers_dir = os.path.join(assets_dir, "markers", name)
  _, markers = load_images_and_markers(markers_dir)
  orig_image_fns = sorted(glob.glob(os.path.join(markers_dir, "*.png")))

  output_dir = os.path.join(output_dir, name)
  os.makedirs(output_dir, exist_ok=True)

  for marker, orig_fn in zip(markers, orig_image_fns):
    orig_image = cv2.imread(orig_fn, cv2.IMREAD_GRAYSCALE)
    plate_ys, plate_xs = np.where(marker == 1)
    backg_ys, backg_xs = np.where(marker == 2)

    fig, ax = plt.subplots()
    plt.axis('off')
    ax.imshow(orig_image, cmap="gray")
    ax.scatter(plate_xs, plate_ys, s=6)
    ax.scatter(backg_xs, backg_ys, s=6)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(
        os.path.join(output_dir, os.path.basename(orig_fn)),
        bbox_inches="tight",
        pad_inches=0)
    plt.close(fig)


if __name__ == "__main__":
  fire.Fire(vis_markers)

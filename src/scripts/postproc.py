import os

import cv2
import fire
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from src.util import load_obj
from tqdm import tqdm


def extract_patch(center, shape, img):
  y, x = center
  h, w = shape
  return img[y - h // 2:y + h // 2, x - w // 2:x + w // 2]


def get_orig_img(img_id):
  orig_fn = os.path.join("assets", "plates", f"orig_{img_id}.png")
  return cv2.imread(orig_fn, cv2.IMREAD_GRAYSCALE)


def get_mask_img(img_id):
  orig_fn = os.path.join("assets", "plates", f"mask_{img_id}.png")
  return cv2.imread(orig_fn, cv2.IMREAD_GRAYSCALE)


def fname_to_img_id(filename):
  basename = os.path.basename(filename)
  return basename.split("_")[2]


def postproc(exp_dir):
  output_dir = os.path.join(exp_dir, "postproc")
  os.makedirs(output_dir, exist_ok=True)
  split_intersec = []
  for split in tqdm(range(1, 4), desc="Processing split"):
    # Get dictionary from filename to prediction
    results_fn = os.path.join(exp_dir, f"split-{split}-results.csv")
    results = pd.read_csv(results_fn, index_col="filenames")
    preds_dict = results["preds"].to_dict()

    # Get dictionary from filename to center coordiante
    coords_fn = os.path.join(exp_dir, f"test-{split}", "patch-coords")
    coords_dict = load_obj(coords_fn)

    img_ids = set(map(fname_to_img_id, coords_dict.keys()))
    intersec_ratios = []
    for img_id in tqdm(img_ids, desc="Processing image ids"):
      plate_patch_keys = list(
          filter(lambda k: fname_to_img_id(k) == img_id and preds_dict[k] == 1,
                 coords_dict.keys()))
      plate_coords_yx = [coords_dict[pk] for pk in plate_patch_keys]
      fig, ax = plt.subplots()
      plt.axis("off")
      img = get_orig_img(img_id)
      ax.imshow(img, cmap="gray")

      # Compute coordinates for patch centers and plot mean
      xs = [c[1] for c in plate_coords_yx]
      ys = [c[0] for c in plate_coords_yx]
      mean_x = np.mean(xs)
      mean_y = np.mean(ys)
      ax.scatter(xs, ys)
      ax.scatter(mean_x, mean_y)

      ax.get_xaxis().set_visible(False)
      ax.get_yaxis().set_visible(False)
      fig.savefig(
          os.path.join(output_dir, f"{img_id}.png"),
          bbox_inches="tight",
          pad_inches=0.0)
      plt.close(fig)

      mask_img = get_mask_img(img_id)
      try:
        mask_patch = extract_patch(
            center=(round(mean_y), round(mean_x)),
            shape=(69, 135),
            img=mask_img)
      except Exception:
        import ipdb
        ipdb.set_trace()
      intersec = np.sum(mask_patch == 255) / np.sum(mask_img == 255)
      intersec_ratios.append(intersec)
    split_intersec.append(np.mean(intersec_ratios))
  with open(os.path.join(output_dir, "eval.txt"), "w") as f:
    mean_intersec = np.mean(split_intersec)
    std_intersec = np.std(split_intersec) * 1.96
    f.write(f"{mean_intersec:.5f} ± {std_intersec:.5f}\n")


if __name__ == "__main__":
  fire.Fire(postproc)

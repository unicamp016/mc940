import os
from collections import defaultdict
from functools import cached_property

import fire
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import auc, roc_curve
from tqdm import tqdm

matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem":
        "pdflatex",
    "font.family":
        "serif",
    "text.usetex":
        True,
    "pgf.rcfonts":
        False,
    "pgf.preamble":
        r" ".join([
            r"\usepackage[utf8x]{inputenc}",
            r"\usepackage[T1]{fontenc}",
            r"\usepackage{amsmath}",
        ])
})


class MetricCalculator:

  def __init__(self, preds, labels, probs=None):
    assert len(preds) == len(labels)
    assert probs is None or len(preds) == len(probs)
    self.preds = preds
    self.labels = labels
    self.probs = probs

  def __len__(self):
    """Return number of samples being evaluated."""
    return len(self.preds)

  @cached_property
  def tp(self):
    """Compute true positives."""
    return np.sum(np.logical_and(self.labels == 1, self.preds == 1))

  @cached_property
  def tn(self):
    """Compute true negatives."""
    return np.sum(np.logical_and(self.labels == 0, self.preds == 0))

  @cached_property
  def fp(self):
    """Compute false positives."""
    return np.sum(np.logical_and(self.labels == 0, self.preds == 1))

  @cached_property
  def fn(self):
    """Compute false negatives."""
    return np.sum(np.logical_and(self.labels == 1, self.preds == 0))

  @cached_property
  def accuracy(self):
    """Compute accuracy."""
    return (self.tp + self.tn) / len(self)

  @cached_property
  def precision(self):
    """Compute precision."""
    return 0 if self.tp == 0 else self.tp / (self.tp + self.fp)

  @cached_property
  def recall(self):
    """Compute recall."""
    return 0 if self.tp == 0 else self.tp / (self.tp + self.fn)

  @cached_property
  def f1(self):
    """Compute F1 score."""
    if self.precision == 0 or self.recall == 0:
      return 0
    return 2 * (self.precision * self.recall) / (self.precision + self.recall)

  @cached_property
  def fpr_tpr(self):
    """Compute false positive and true positive rates for ROC curve."""
    assert self.probs is not None, "Can't compute AUC without prob scores."
    fpr, tpr, _ = roc_curve(self.labels, self.probs)
    return (fpr, tpr)

  @cached_property
  def auc(self):
    """Compute the Area Under the ROC (AUC)."""
    fpr, tpr = self.fpr_tpr
    return auc(fpr, tpr)


def evaluate(*exp_dirs):
  calculators_list = []
  for exp_dir in exp_dirs:
    # Keep track of metrics for each split.
    calculators = []

    # Metrics we wish to compute.
    metrics = ["accuracy", "precision", "recall", "f1"]

    for split in tqdm(range(1, 4)):
      results_fn = os.path.join(exp_dir, f"split-{split}-results.csv")
      df = pd.read_csv(results_fn)
      # If probs is available, compute AUC.
      probs = None
      if "probs" in df:
        probs = df["probs"].values
        if "auc" not in metrics:
          metrics.append("auc")
      calculators.append(
          MetricCalculator(
              preds=df["preds"].values, labels=df["labels"].values,
              probs=probs))
    with open(os.path.join(exp_dir, "eval.txt"), "w") as f:
      for m in metrics:
        values = [getattr(c, m) for c in calculators]
        mean = np.mean(values)
        std = np.std(values)
        f.write(f"{m:10s}| {mean:.5f} ± {std * 1.96:.5f}\n")
    calculators_list.append(calculators)

  # Plot the ROC curve with error bars for the given experiment.
  with open("tab.tex", "w") as f:
    table_dict = defaultdict(list)
    for exp_dir, calculators in zip(exp_dirs, calculators_list):
      exp_name = os.path.basename(exp_dir)
      table_dict["names"].append(exp_name)
      for m in metrics:
        values = [getattr(c, m) for c in calculators]
        mean_val, err_val = np.mean(values), np.std(values) * 1.96
        if m == "accuracy":
          mean_val *= 100
          err_val *= 100
        table_dict[f"{m}-mean"].append(mean_val)
        table_dict[f"{m}-err"].append(err_val)
    for i in range(len(exp_dirs)):
      exp_name = table_dict["names"][i]
      f.write(f"{exp_name:15s} ")
      for m in metrics:
        mean_val = table_dict[f"{m}-mean"][i]
        err_val = table_dict[f"{m}-err"][i]
        is_bold = mean_val == np.max(table_dict[f"{m}-mean"])

        if is_bold:
          write_str = f"& $\\mathbf{{{mean_val:.3f} \\pm {err_val:.3f}}}$"
        else:
          write_str = f"& ${mean_val:.3f} \\pm {err_val:.3f}$"
        f.write(f"{write_str:35s}")
      f.write("\\\\\n")

  if "auc" in metrics:
    fig, ax = plt.subplots(figsize=(7, 7))
    fpr_mean = np.linspace(0, 1, 200)
    for exp_dir, calculators in zip(exp_dirs, calculators_list):
      exp_name = os.path.basename(exp_dir)
      tprs = [
          np.interp(fpr_mean, c.fpr_tpr[0], c.fpr_tpr[1]) for c in calculators
      ]
      tpr_mean = np.mean(tprs, axis=0)
      tpr_std = np.std(tprs, axis=0) * 1.96
      ax.plot(fpr_mean, tpr_mean, label=exp_name)
      ax.fill_between(
          fpr_mean, tpr_mean + tpr_std, tpr_mean - tpr_std, alpha=0.3)
    ax.plot([0, 1], [0, 1], color="gray", linestyle="--")
    ax.set_xlim([0.0, 1.0])
    ax.set_ylim([0.0, 1.05])
    ax.set_xlabel("False Positive Rate")
    ax.set_ylabel("True Positive Rate")
    ax.legend(loc="lower right")
    fig.savefig("roc.pdf")
    # fig.waitforbuttonpress()
    plt.close(fig)


if __name__ == "__main__":
  fire.Fire(evaluate)

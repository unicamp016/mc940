# pylint: disable=invalid-name
"""Utility functions."""
import csv
import enum
import glob
import itertools
import math
import os
import pickle

import cv2
import matplotlib.cm as cm
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np
import pyift.pyift as ift
from flim.experiments.utils import load_markers
from flim.models.lcn import LCNCreator
from scipy.signal import convolve2d
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

from src.consts import DEVICE

eps = np.finfo(np.float32).eps

# Smoothing kernel
S = np.array([[1, 2, 1]])

# Gradient kernel
G = np.array([[1, 0, -1]])

CMAP = cm.get_cmap("jet")
BACKGROUND = 0
FOREGROUND = 1


def gray_to_rgb(gray):
  """Convert a grayscale image to an RGB image using the default colormap."""
  rgba = CMAP(gray, bytes=True)
  return rgba[..., :3]


def sobel_filter(kernel_size, direction="x"):
  """Return a sobel filter for the specified kernel size and direction."""
  assert direction in ["x", "y"]
  assert kernel_size % 2 == 1 and kernel_size > 1
  if kernel_size == 3:
    return S.T * G if direction == "x" else S * G.T
  return convolve2d(S * S.T, sobel_filter(kernel_size - 2, direction))


def kl_div(pred, mask):
  """Compute the Kullback-Leibler Divergence (KL-Div) between pred and mask.

  Args:
    pred: A 2-D numpy array with the activations that should be brighter in
      regions where there is a plate.
    mask: A 2-D binary numpy array with 1s in the plate locations and 0
      everywhere else.
  """
  rel_entropy = mask * np.log(eps + mask / (eps + pred))
  return np.sum(rel_entropy)


def nss(pred, mask):
  """Compute the Normalized Scanpath Saliency (NSS) between the pred and mask.

  Args:
    pred: A 4-D numpy array with the activations that should be brighter in
      regions where there is a plate and shape of the form
        (batch_size, height, weight, channels)
    mask: A 3-D binary numpy array with 1s in the plate locations, 0
      everywhere else and shape of the form
        (batch_size, height, weight)

  Returns:
    A numpy array with shape (batch_size, channels) with the NSS score
    for every image in the batch between the mask and every channel in the input
    data.
  """
  pred_mean = np.mean(pred, axis=(1, 2), keepdims=True)
  pred_std = np.mean(pred, axis=(1, 2), keepdims=True)
  zpred = (pred - pred_mean) / pred_std
  N = np.sum(mask, axis=(1, 2), keepdims=True)
  mask /= N
  return np.sum(zpred * np.expand_dims(mask, axis=-1), axis=(1, 2))


def normalize(pred, mask):
  """Normalizes pred and mask between 0 and 1 and matches their dimensions.

  Args:
    pred: A 4-D numpy array with shape (batch_size, height, width, channels)
      that only has positive values. This method should only be called after
      applying a ReLU, for example.
    mask: A 3-D numpy array with shape (batch_size, height, width) with binary
      values that are either 0 or 255.
  """
  pred_max = np.maximum(np.max(pred, axis=(1, 2), keepdims=True), 1)
  pred /= pred_max
  mask = mask.astype(np.float32)
  mask_max = np.maximum(np.max(mask, axis=(1, 2), keepdims=True), 1)
  mask /= mask_max
  mask = np.expand_dims(mask, axis=-1)
  return pred, mask


def l2_norm(pred, mask, mask_weights):
  """Compute the L2 norm between the unit-normalized prediction and mask.

  Before adding the individual errors between each pixel, the error is
  scaled by the weights in `mask_weights`. The final norm for each image is:
    l2 = sqrt(sum(w_p * (pred_p - mask_p) ** 2))
  """
  pred = rescale(pred, minval=0, maxval=1)
  mask = mask / 255
  mask = np.expand_dims(mask, axis=-1)
  mask_weights = mask_weights / 255
  mask_weights = np.expand_dims(mask_weights, axis=-1)
  return np.sqrt(np.sum(mask_weights * (pred - mask)**2, axis=(1, 2)))


def l1_norm(pred, mask, mask_weights):
  """Compute the L1 norm between the unit-normalized prediction and mask.

  Before adding the individual errors between each pixel, the error is
  scaled by the weights in `mask_weights`.
    l1 = sum(w_p * abs(pred_p - mask_p))
  """
  pred = rescale(pred, minval=0, maxval=1)
  mask = mask.astype(np.float32) / 255
  mask = np.expand_dims(mask, axis=-1)
  mask_weights = mask_weights.astype(np.float32) / 255
  mask_weights = np.expand_dims(mask_weights, axis=-1)
  return np.sum(mask_weights * np.abs(pred - mask), axis=(1, 2))


class Metric(enum.Enum):
  """The different metrics that we can compute to evaluate a batch."""

  nss = "nss"  # Normalized Scanpath Saliency
  kl = "kl"  # KL divergence
  l2 = "l2"  # The L2-norm or Euclidian distance
  l1 = "l1"  # The L1-norm or absolute difference


def worst_case_metric_val(shape, metric):
  """Compute the worst case scenario value for the given metric.

  Args:
    shape: The shape of the images used for the worst case scenario evaluation.
    metric: The Metric type we wish to evaluate.
  """
  pred_zeros = np.expand_dims(np.zeros(shape), axis=[0, -1])
  weight_ones = np.expand_dims(255 * np.ones(shape), axis=0)
  mask_ones = np.expand_dims(255 * np.ones(shape), axis=0)
  worst_val = None
  if metric is Metric.l2:
    worst_val = l2_norm(pred_zeros, mask_ones, weight_ones)
  if metric is Metric.l1:
    worst_val = l1_norm(pred_zeros, mask_ones, weight_ones)
  if worst_val is not None:
    return worst_val[0][0]
  raise ValueError("Invalid metric type.")


def evaluate(batch, metric):
  """Evaluate how good a batch is with respect to a metric.

  Args:
    metric: A Metric type with the metric we wish to compute.
  """
  if metric is Metric.nss:
    return nss(batch, batch.mask_data)
  if metric is Metric.kl:
    return kl_div(batch, batch.mask_data)
  if metric is Metric.l2:
    return l2_norm(batch, batch.mask_data, batch.mask_weights)
  if metric is Metric.l1:
    return l1_norm(batch, batch.mask_data, batch.mask_weights)
  raise ValueError(f"The evaluation of the {metric.value} metric hasn't"
                   "been implemented yet.")


class Batch(np.ndarray):
  """Batch of data with the respective filenames for each sample."""

  def __new__(cls, orig_data, mask_data, mask_weights, filenames=None):
    """Initialize a Batch from a numpy array and optional list of filenames."""
    # Cast to be our class type
    obj = np.asarray(orig_data).view(cls)
    obj.filenames = filenames
    obj.mask_data = mask_data
    obj.mask_weights = mask_weights
    return obj

  def __array_finalize__(self, obj):
    if obj is None:
      return
    self.filenames = getattr(obj, "filenames", None)
    self.mask_data = getattr(obj, "mask_data", None)
    self.mask_weights = getattr(obj, "mask_weights", None)

  @staticmethod
  def from_batch(new_data, prev_batch):
    """Initialize a Batch instance with new data and prev_batch's properties."""
    return Batch(
        new_data,
        mask_data=prev_batch.mask_data,
        mask_weights=prev_batch.mask_weights,
        filenames=prev_batch.filenames)


class RunningMean:
  """Keep track of the running mean of a numpy array."""

  def __init__(self):
    self._mean_arr = None
    self.total_weight = 0

  def __call__(self, arr, weight=1):
    """Include `arr` in the running mean with `weight`.

    The array should have the same shape as the running mean.
    """
    self.total_weight += weight
    if self._mean_arr is None:
      self._mean_arr = arr
    else:
      mean_err = (weight / self.total_weight) * (arr - self._mean_arr)
      self._mean_arr = self._mean_arr + mean_err

  def result(self):
    """Return the computed mean array."""
    return self._mean_arr


def create_dir(dirname):
  """Create directory if it doesn't exist.

  Returns:
    True if the directory was created. False if the directory already exists.
  """
  if not os.path.isdir(dirname):
    os.makedirs(dirname)
    return True
  return False


def random_kernel_bank(bands, shape, znorm=False):
  """Create a random kernel bank.

  Args:
    bands: Integer with the number of bands for the kernel bank.
    shape: Tuple with kernel's 3-D shape, which represents the adjacency set
      with respect to the center pixel that will be used to convolve the image.
    znorm: Boolean indicating whether to force each kernel band to have zero
      mean and unit variance.

  Returns:
    A list of `bands` kernels with shape (shape[0], shape[1]) with random
    weights.
  """
  kernel_bank = np.random.random((bands,) + tuple(shape))
  if znorm:
    band_std = np.std(kernel_bank, ddof=1, axis=(1, 2, 3), keepdims=True)
    band_mean = np.mean(kernel_bank, axis=(1, 2, 3), keepdims=True)
    kernel_bank = (kernel_bank - band_mean) / band_std
  return list(kernel_bank)


def replace_parent_dir(fn, new_parent_dir):
  parent_dir = os.path.basename(os.path.dirname(fn))
  new_fn = fn.replace(parent_dir, new_parent_dir)
  return new_fn


def rescale(batch, minval=0, maxval=255):
  """Rescale array between [minval, maxval].

  If any image in the batch consists of the same value throughout, an image with
  all elements equal to minval is returned.
  """
  batch = batch.astype(np.float32)
  batch_min = np.min(batch, axis=(1, 2), keepdims=True)
  batch_max = np.max(batch, axis=(1, 2), keepdims=True)
  batch_range = batch_max - batch_min
  unit_batch = np.divide(
      batch - batch_min,
      batch_range,
      out=np.zeros_like(batch),
      where=batch_range != 0,
  )
  norm_batch = (maxval - minval) * unit_batch + minval
  return norm_batch


def save_batch(batch, dirname, save_channels=True):
  """Save a batch of data to the specified dirname.

  Args:
    batch: A data.Batch object with the sample stacked along the first axis.
    dirname: The directory name under which the batch samples will be saved.
    save_channels: If True, saves each channel as a separate grayscale image.
      Otherwise, attempts to save the entire sample as a png.
  """
  assert batch.dtype == np.uint8
  assert np.max(batch) == 255 and np.min(batch) == 0
  new_dir = os.path.dirname(replace_parent_dir(batch.filenames[0], dirname))
  create_dir(new_dir)
  for sample, sample_fn in zip(batch, batch.filenames):
    new_fn = replace_parent_dir(sample_fn, dirname)
    if save_channels:
      new_fn_basename, new_fn_ext = os.path.splitext(new_fn)
      for c in range(sample.shape[-1]):
        sample_channel = sample[..., c]
        new_fn = f"{new_fn_basename}_{c:03d}{new_fn_ext}"
        cv2.imwrite(new_fn, sample_channel)
    else:
      # TODO: Verify if we don't need to perform a color conversion from RGB
      # to BGR here.
      cv2.imwrite(new_fn, sample)


def sliding_window_patch_gen(orig, mask, _, plate_info):
  """Return a generator that yields superpixel patches given the image.

  Args:
    orig: The original grayscale image of a plate that we wish to break into
      patches.
    mask: The respective mask for the plate image with the same dimensions.
    patch_params: The extracted plate parameters to use for patch extraction. A
      4-tuple with the patch height, width, dy, and dx.

  Yields:
    Tuples with the original and respective mask patches for the given image.
  """
  img_h, img_w = orig.shape
  patch_h, patch_w, dy, dx = plate_info
  range_y = range(0, img_h - patch_h, dy)
  range_x = range(0, img_w - patch_w, dx)
  for oy, ox in itertools.product(range_y, range_x):
    center = (oy + patch_h // 2, ox + patch_w // 2)
    orig_patch = orig[oy:oy + patch_h, ox:ox + patch_w]
    mask_patch = mask[oy:oy + patch_h, ox:ox + patch_w]
    yield orig_patch, mask_patch, center


def superpixel_patch_gen(orig, mask, orig_fn, patch_params, num_init_seeds,
                         num_superpixels):
  """Return a generator that yields superpixel patches given the image.

  Computes the superpixel image and extracts a patch from the superpixel
  centroid.

  Args:
    orig: The original grayscale image of a plate that we wish to break into
      patches.
    mask: The respective mask for the plate image with the same dimensions.
    patch_params: The extracted plate parameters to use for patch extraction. A
      4-tuple with the patch height, width, dy, and dx.
    num_init_seeds: The number of initial seeds used to extract superpixels
      in the DISF algorithm.
    num_superpixels: The number of superpixels we want to extract.

  Yields:
    Tuples with the original and respective mask patches for the given image.
  """
  dirname = os.path.basename(os.path.dirname(orig_fn))
  supxl_fn = orig_fn.replace(dirname, "supxl")
  create_dir(os.path.dirname(supxl_fn))
  h, w = orig.shape
  ph, pw, _, _ = patch_params
  supxl = compute_superpixels(orig, num_init_seeds, num_superpixels)
  supxl_fig = overlay(orig, supxl, patch_params)
  supxl_fig.savefig(supxl_fn, bbox_inches="tight", pad_inches=0)
  plt.close(supxl_fig)
  for s in range(1, np.max(supxl) + 1):
    ys, xs = np.nonzero(supxl == s)
    cx, cy = int(np.mean(xs)), int(np.mean(ys))
    ox = cx - pw // 2
    oy = cy - ph // 2
    if ox < 0 or oy < 0 or ox + pw > w or oy + ph > h:
      continue
    center = (oy + ph // 2, ox + pw // 2)
    orig_patch = orig[oy:oy + ph, ox:ox + pw]
    mask_patch = mask[oy:oy + ph, ox:ox + pw]
    yield orig_patch, mask_patch, center


def rectangular_adjacency(h, w):
  """Return a rectangular adjacency with height `h` and width `w`.

  If an even height or width are given, the adjacency always prioritizes the
  top and left side before filling the bottom and right sides.
  """
  A = []
  w_min = -(w // 2)
  h_min = -(h // 2)
  for w_i in range(w_min, w_min + w):
    for h_i in range(h_min, h_min + h):
      A.append([h_i, w_i])
  return np.array(A)


def circular_adjacency(r):
  """Return an array of index pairs for a circular adjacency with radius `r`."""
  ds = tuple(range(math.floor(-r), math.ceil(r) + 1))
  dxdys = itertools.product(ds, ds)
  A = filter(lambda dxdy: 0 < np.linalg.norm(dxdy) <= r, dxdys)
  return np.array(list(A))


def compute_superpixels(image, num_init_seeds=400, num_superpixels=50):
  """Compute the superpixel labels from a grayscale image."""
  rgb_cmap = cm.get_cmap("jet")
  rgba = rgb_cmap(image, bytes=True)
  rgb = rgba[..., :3]
  # rgb = rgb.astype(np.float32)
  lab = cv2.cvtColor(rgb, cv2.COLOR_RGB2LAB)
  A = ift.Circular(np.sqrt(2.0))
  lab_ift = ift.CreateMImageFromNumPy(lab)
  supxl_ift = ift.DISF(lab_ift, A, num_init_seeds, num_superpixels, None)
  supxl = supxl_ift.AsNumPy()
  return supxl


def overlay(image, label, patch_params, alpha=0.65):
  """Apply a colormap to labels superpose it with image."""
  ph, pw, _, _ = patch_params
  h, w = image.shape
  fig, ax = plt.subplots()
  plt.axis('off')
  ax.imshow(image, cmap="gray")
  ax.imshow(label, cmap="inferno", alpha=alpha)
  ax.get_xaxis().set_visible(False)
  ax.get_yaxis().set_visible(False)
  for l in range(1, np.max(label) + 1):
    ys, xs = np.nonzero(label == l)
    cx, cy = np.mean(xs), np.mean(ys)
    center = patches.Circle((cx, cy), radius=2, color="red", alpha=0.6)
    ax.add_patch(center)
    ox, oy = cx - pw // 2, cy - ph // 2
    if ox < 0 or oy < 0 or ox + pw > w or oy + ph > h:
      continue
    rect = patches.Rectangle((ox, oy),
                             pw,
                             ph,
                             edgecolor="magenta",
                             alpha=0.6,
                             facecolor="none")
    ax.add_patch(rect)
  return fig


def load_architecture(bands, kernel_size):
  """Return a dictionary with a valid LIDSConvNet architecture."""
  return {
      "features": {
          "type": "sequential",
          "layers": {
              "conv1": {
                  "operation": "conv2d",
                  "params": {
                      "kernel_size": kernel_size,
                      "stride": 1,
                      "padding": kernel_size // 2,
                      "dilation": 1,
                      "number_of_kernels_per_marker": bands // 2
                  }
              },
              "activation": {
                  "operation": "relu",
                  "params": {
                      "inplace": True
                  }
              },
              "pool": {
                  "operation": "max_pool2d",
                  "params": {
                      "kernel_size": 3,
                      "stride": 1,
                      "padding": 1
                  }
              },
              "norm1": {
                  "operation": "batch_norm2d",
                  "params": {}
              }
          }
      }
  }


def load_images_and_markers(markers_dir):
  """Load images and markers from directory."""
  image_fns = sorted(glob.glob(os.path.join(markers_dir, "*.png")))
  marker_fns = sorted(glob.glob(os.path.join(markers_dir, "*.txt")))
  images, markers = [], []
  for image_fn, marker_fn in zip(image_fns, marker_fns):
    markers.append(load_markers(marker_fn))
    gray = cv2.imread(image_fn, cv2.IMREAD_GRAYSCALE)
    rgb = gray_to_rgb(gray)
    lab = cv2.cvtColor(rgb, cv2.COLOR_RGB2LAB)
    images.append(lab)
  return np.stack(images, axis=0), np.stack(markers, axis=0)


def load_flim_model(markers_path, bands, kernel_size):
  """Load the flim kernels for the markers that match the current strategy."""
  images, markers = load_images_and_markers(markers_path)
  creator = LCNCreator(
      load_architecture(bands, kernel_size),
      images=images,
      markers=markers,
      relabel_markers=False,
      device=DEVICE)
  creator.build_feature_extractor()
  model = creator.get_LIDSConvNet()
  return model


def load_kernel_bank(train_split, bands, kernel_size, kernel_norm):
  """Load the kernel bank from the given file or create a new one and save."""
  rgb_sobel_x = np.repeat(
      np.expand_dims(sobel_filter(kernel_size, 'x'), axis=-1), 3, axis=-1)
  rgb_sobel_y = np.repeat(
      np.expand_dims(sobel_filter(kernel_size, 'y'), axis=-1), 3, axis=-1)
  baseline_kb = [rgb_sobel_x, rgb_sobel_y]
  random_kb = random_kernel_bank(
      bands=bands - 2, shape=(kernel_size, kernel_size, 3), znorm=kernel_norm)
  kernel_bank = np.stack(baseline_kb + random_kb)
  np.save(os.path.join(train_split.split_dir, "kernels.npy"), kernel_bank)
  return kernel_bank


def compute_mask_weights(mask, min_weight_ratio=0.05):
  """Compute the mask weights in the range [0, 255] used to evaluate results.

  Args:
    mask: A 2-D binary numpy array with 0s for background pixels and 255 for
      foreground pixels.
    min_weight_ratio: The relative weight of the background pixels with respect
      to the foreground ones. We compute the maximum value in the mask and
      multipy it by minval to determine the minimum weight assigned to a pixel.
      Pixels close to the plate have a higher weight due to the gaussian blur.

  Returns:
    A 2-D numpy array with the same dimensions as mask with values ranging from
      [0, 1].
  """
  mask_weights = cv2.GaussianBlur(mask, ksize=(51, 51), sigmaX=9)
  clipped_mask_weights = np.maximum(min_weight_ratio * 255, mask_weights)
  return clipped_mask_weights.astype(np.uint8)


def weighted_region_coords(image_shape, kernel_shape, feat_type):
  """Extract the regions and weights to compute the per-pixel haar feature.

  Args:
    image_shape: The shape of the image we wish to apply the haar feature to
      (h, w).
    kernel_shape: The kernel_shape used to compute the feature values in the
      form (k_h, k_w).
    feat_type: The feature id we wish to compute. One of "type-3-y", "type-3-x"
      or "plate".

  Returns:
    A tuple with a list of lists of bounding boxes of each region used to
    compute the feature and a list of weights applied to each bounding box.
  """
  h, w = image_shape
  w_h, w_w = kernel_shape
  if feat_type == "type-3-y":
    assert w_h % 3 == 0
    weights = [-1, 2, -1]
    regions = [[] for _ in weights]
    r_h, r_w = w_h // 3, w_w
    for i in range(h):
      for j in range(w):
        regions[0].append([(i, j), (i + r_h - 1, j + r_w - 1)])
        regions[1].append([(i + r_h, j), (i + 2 * r_h - 1, j + r_w - 1)])
        regions[2].append([(i + 2 * r_h, j), (i + 3 * r_h - 1, j + r_w - 1)])
  elif feat_type == "type-3-x":
    assert w_w % 3 == 0
    weights = [-1, 2, -1]
    regions = [[] for _ in weights]
    r_h, r_w = w_h, w_w // 3
    for i in range(h):
      for j in range(w):
        regions[0].append([(i, j), (i + r_h - 1, j + r_w - 1)])
        regions[1].append([(i, j + r_w), (i + r_h - 1, j + 2 * r_w - 1)])
        regions[2].append([(i, j + 2 * r_w), (i + r_h - 1, j + 3 * r_w - 1)])
  elif feat_type == "plate":
    assert w_h % 7 == 0 and w_w % 7 == 0
    weights = [-1, -1, -1, -1, 4]
    regions = [[] for _ in weights]
    v_margin_h = w_h // 7
    h_margin_w = w_w // 7
    for i in range(h):
      for j in range(w):
        regions[0].append([(i, j), (i + v_margin_h - 1, j + w_w - 1)])
        regions[1].append([(i + w_h - v_margin_h, j),
                           (i + w_h - 1, j + w_w - 1)])
        regions[2].append([(i + v_margin_h, j),
                           (i + w_h - v_margin_h - 1, j + h_margin_w - 1)])
        regions[3].append([(i + v_margin_h, j + w_w - h_margin_w),
                           (i + w_h - v_margin_h, j + w_w - 1)])
        regions[4].append([(i + v_margin_h, j + h_margin_w),
                           (i + w_h - v_margin_h - 1, j + w_w - h_margin_w - 1)
                          ])
  else:
    raise ValueError(f"Unsupported Haar feature type {feat_type}")
  return regions, weights


def batch_labels(batch):
  """Return the labels for a Batch object."""
  return np.array(list(map(filename_to_label, batch.filenames)))


def filename_to_label(filename):
  """Return a label from a filename."""
  basename = os.path.basename(filename)
  return int(basename.split("_")[0])


def draw_all_haar_feat_types(train_split):
  """Draw all existing feature types to training split directory."""
  draw_haar_feat_type(
      os.path.join(train_split.split_dir, "type-3-y-3x3.pdf"),
      kernel_shape=(3, 3),
      feat_type="type-3-y")
  draw_haar_feat_type(
      os.path.join(train_split.split_dir, "type-3-y-9x9.pdf"),
      kernel_shape=(9, 9),
      feat_type="type-3-y")
  draw_haar_feat_type(
      os.path.join(train_split.split_dir, "type-3-x-3x3.pdf"),
      kernel_shape=(3, 3),
      feat_type="type-3-x")
  draw_haar_feat_type(
      os.path.join(train_split.split_dir, "type-3-x-9x9.pdf"),
      kernel_shape=(9, 9),
      feat_type="type-3-x")
  draw_haar_feat_type(
      os.path.join(train_split.split_dir, "plate-35x70.pdf"),
      kernel_shape=(35, 70),
      feat_type="plate")
  draw_haar_feat_type(
      os.path.join(train_split.split_dir, "plate-35x35.pdf"),
      kernel_shape=(35, 35),
      feat_type="plate")


def draw_haar_feat_type(filename, kernel_shape, feat_type):
  """Draw the kernel for the selected haar feature and save pdf to filename."""
  regions, weights = weighted_region_coords((1, 1), kernel_shape, feat_type)
  fig, ax = plt.subplots()
  background = np.zeros(kernel_shape, dtype=np.uint8)
  ax.imshow(background, extent=[0, kernel_shape[1], kernel_shape[0], 0])
  for region, weight in zip(regions, weights):
    origin_yx, diag_yx = region[0]
    origin_xy = origin_yx[::-1]
    height = diag_yx[0] - origin_yx[0] + 1
    width = diag_yx[1] - origin_yx[1] + 1
    if weight < 0:
      color = "black"
    else:
      color = "white"
    rect = patches.Rectangle(origin_xy, width, height, color=color, fill=True)
    ax.add_patch(rect)
  fig.savefig(filename, bbox_inches="tight", pad_inches=0.1)


def write_csv(filename, csvdata):
  """Write a csv file from a dictionary that maps keys to iterables."""
  with open(filename, 'w', newline='') as csvfile:
    fieldnames = csvdata.keys()
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for values in zip(*csvdata.values()):
      writer.writerow(dict(zip(fieldnames, values)))


def save_obj(obj, filename):
  """Save object as pickle to `filename`."""
  with open(f"{filename}.pkl", 'wb') as f:
    pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(filename):
  """Load pickle object from `filename`."""
  with open(f"{filename}.pkl", "rb") as f:
    return pickle.load(f)


def plot_feat_space(X, labels, filename, method="pca", perplexity=50):
  """Plot feature space for the given `X` dataset and save to `filename`.

  Args:
    X: A numpy array with shape (N, ...) where N is the number of samples.
      The array is always flattened, so it can have arbitrary dimensions, as
      long as the samples dimensions is the first one.
    labels: X dataset labels used to distinguish samples in the visualization.
    filename: The base filename to save the visualizations to.
    method: The method used to reduce feature dimensionality. One of 'pca or
      'tsne'.
    perplexity: t-SNE parameter that should be tuned according to your data.
      It's related to the number of nearest neighbors in the manifold of the
      feature space. Larger datasets typically require larger perplexity values.
      Only used if `method` is 'tsne'.
  """
  assert method in ["pca", "tsne"]
  assert X.shape[0] == labels.shape[0]

  # Flatten the data
  X = X.reshape(X.shape[0], -1)

  if method == "tsne":
    # Reduce dimensionality before feeding to t-SNE
    if X.shape[-1] > 50:
      pca = PCA(n_components=50)
      X = pca.fit_transform(X)
    tsne = TSNE(n_components=2, verbose=1, perplexity=perplexity)
    Y = tsne.fit_transform(X)
  else:
    pca = PCA(n_components=2)
    Y = pca.fit_transform(X)
  plate = labels == FOREGROUND
  backg = labels == BACKGROUND
  fig, ax = plt.subplots(figsize=(5, 5))
  plt.axis('off')
  ax.scatter(
      Y[backg, 0], Y[backg, 1], s=1, c="r", alpha=0.5, label="background")
  ax.scatter(Y[plate, 0], Y[plate, 1], s=1, c="g", alpha=0.7, label="plate")
  ax.get_xaxis().set_visible(False)
  ax.get_yaxis().set_visible(False)
  fig.savefig(filename, bbox_inches="tight", pad_inches=0)

# pylint: disable=invalid-name, cell-var-from-loop
"""Data processing module to handle saving and reading the plates."""
import enum
import glob
import math
import os
import shutil

import cv2
import numpy as np
from tqdm import tqdm

from src import util


class SplitType(enum.Enum):
  """Different types of dataset splits."""

  train = "train"
  test = "test"


class DatasetSplit:

  def __init__(self,
               split_id,
               split_type,
               assets_dir,
               output_dir,
               patch_padding=0.2,
               displacement_ratio=0.8,
               plate_threshold=0.85,
               overwrite=False):
    """Initialize a train or test split for the dataset.

    Args:
      split_id: An integer with the id of the split we wish to process.
      split_type: A SplitType enum that can be either test or train.
      assets_dir: The directory from where we should read the split files.
      output_dir: The directory where we should save the dataset split
        artifacts.
      patch_padding: The amount of padding added to the smallest bounding box
        that can encompass any plate in the training split.
      displacement_ratio: The percentage of the minimum plate width and height
        in the dataset that will be used as a displacement between patches in
        a dataset.
      plate_threshold: The percentage of pixels from the plate that must be
        within a patch for it to be classified as a plate instead of background
        patch.
    """
    self.split_type = split_type
    self._assets_dir = assets_dir
    self._output_dir = output_dir
    self._split_id = split_id
    self._split_type = split_type
    self._overwrite = overwrite
    self._split_dir = None
    self._patch_padding = patch_padding
    self._displacement_ratio = displacement_ratio
    self._plate_threshold = plate_threshold
    self._split_fn = os.path.join(self._assets_dir, "splits",
                                  f"{self.split_type.value}{split_id}.txt")
    self._orig_fns = []
    self._mask_fns = []
    self._plate_info = None

  @property
  def orig_fns(self):
    """Return the list of filenames for the original images in the split."""
    if not self._orig_fns:
      with open(self._split_fn, "r") as split_f:
        self._orig_fns = [
            os.path.join(self._assets_dir, fn.strip())
            for fn in split_f.readlines()
        ]
    return self._orig_fns

  @property
  def mask_fns(self):
    """Return the list of filenames for the image masks in the split."""
    if not self._mask_fns:
      self._mask_fns = [
          orig_fn.replace("orig", "mask") for orig_fn in self.orig_fns
      ]
    return self._mask_fns

  @property
  def plate_info(self):
    """Compute the maximum and minimum plate dimensions for a given split.

    Returns:
      A 4-tuple with the maximum and minimum height and width for the plates
      in the dataset split. The info is returned in the order
        H_max, W_max, H_min, W_min
    """
    if self.split_type is not SplitType.train:
      raise AttributeError(
          "Plate info extraction is only available for train splits.")

    if self._plate_info is None:
      h_min, w_min = np.inf, np.inf
      h_max, w_max = -np.inf, -np.inf
      for mask_fn in tqdm(self.mask_fns, desc="Computing plate info"):
        mask = cv2.imread(mask_fn, flags=cv2.IMREAD_GRAYSCALE)
        plate_ys, plate_xs = np.nonzero(mask)
        h = np.max(plate_ys) - np.min(plate_ys)
        w = np.max(plate_xs) - np.min(plate_xs)
        if h > h_max:
          h_max = h
        if w > w_max:
          w_max = w
        if h < h_min:
          h_min = h
        if w < w_min:
          w_min = w
      self._plate_info = (h_max, w_max, h_min, w_min)
    return self._plate_info

  @property
  def patch_params(self):
    """Parameters used to extract patches from the original images.

    Returns:
      A 4-tuple with the parameters used to extract patches from the original
      images in the following order:
        H: The height of each patch
        W: The width of each patch
        Dy: The displacement in the height dimension between patches
        Dx: The displacement in the width dimension between patches
    """
    h_max, w_max, h_min, w_min = self.plate_info
    h = math.ceil(h_max * (1 + self._patch_padding))
    w = math.ceil(w_max * (1 + self._patch_padding))
    dy = math.floor(h_min * self._displacement_ratio)
    dx = math.floor(w_min * self._displacement_ratio)
    return h, w, dy, dx

  @property
  def split_dir(self):
    """Return the directory where the split artifacts will be stored."""
    if self._split_dir is None:
      # Assume all original filenames belong to the same directory.
      orig_fn = self.orig_fns[0]
      orig_dirname = os.path.dirname(orig_fn)
      orig_dirname = orig_dirname.replace(self._assets_dir, self._output_dir)
      orig_parent_dir = os.path.basename(orig_dirname)
      self._split_dir = orig_dirname.replace(
          orig_parent_dir, f"{self._split_type.value}-{self._split_id}")
      if self._overwrite and os.path.isdir(self._split_dir):
        shutil.rmtree(self._split_dir)
      os.makedirs(self._split_dir, exist_ok=True)
    return self._split_dir

  def save_patches(self,
                   patch_fn,
                   orig_dirname="patches",
                   mask_dirname="masks"):
    """Save the patches generated by patch_fn to the specified directories.

    For every saved patch, the respective mask is also saved to the
    `mask_dirname` directory. The patch names are the same as the original files
    with a suffix indicating the patch number for the given image and a prefix
    indicating whether the patch belongs to the plate or background class. The
    filenames are of the form
      patch_fn = {split_dir}/{patch_dirname}/{class}_orig_{img_id}_{patch_id}
      mask_fn = {split_dir}/{mask_dirname}/{class}_mask_{img_id}_{patch_id}

    Args:
      patch_fn: A generator function that yields tuples with the original and
        mask patches given an original and mask image.
      orig_dirname: The directory to save the original patches.
      mask_dirname: The directory to save the mask patches.
    """
    orig_base_dir = os.path.join(self.split_dir, orig_dirname)
    mask_base_dir = os.path.join(self.split_dir, mask_dirname)
    mask_weights_base_dir = os.path.join(self.split_dir,
                                         f"{mask_dirname}-weights")
    created = (
        util.create_dir(orig_base_dir) and util.create_dir(mask_base_dir) and
        util.create_dir(mask_weights_base_dir))
    if created:
      plates_per_img = []
      coords_dict = {}
      for orig_fn, mask_fn in tqdm(
          zip(self.orig_fns, self.mask_fns),
          desc="Saving patches",
          total=len(self.orig_fns)):
        orig_basename, orig_ext = os.path.splitext(os.path.basename(orig_fn))
        orig_basename_fn = \
          lambda index: f"{orig_basename}_{index:04d}{orig_ext}"
        mask_basename, mask_ext = os.path.splitext(os.path.basename(mask_fn))
        mask_basename_fn = \
          lambda index: f"{mask_basename}_{index:04d}{mask_ext}"
        orig = cv2.imread(orig_fn, cv2.IMREAD_GRAYSCALE)
        mask = cv2.imread(mask_fn, cv2.IMREAD_GRAYSCALE)
        total_plate_pixels = np.sum(mask == 255)
        plate_patches = 0
        for i, (orig_patch, mask_patch, coord) in enumerate(
            patch_fn(orig, mask,
                     os.path.join(orig_base_dir, os.path.basename(orig_fn)))):
          patch_plate_pixels = np.sum(mask_patch == 255)
          if patch_plate_pixels / total_plate_pixels >= self._plate_threshold:
            plate_patches += 1
            patch_class = 1
          else:
            patch_class = 0

          orig_patch_fn = os.path.join(orig_base_dir,
                                       f"{patch_class}_{orig_basename_fn(i)}")
          mask_patch_fn = os.path.join(mask_base_dir,
                                       f"{patch_class}_{mask_basename_fn(i)}")
          mask_weights_patch = util.compute_mask_weights(mask_patch)
          mask_weights_patch_fn = os.path.join(
              mask_weights_base_dir, f"{patch_class}_{mask_basename_fn(i)}")
          coords_dict[orig_patch_fn] = coord
          cv2.imwrite(orig_patch_fn, orig_patch)
          cv2.imwrite(mask_patch_fn, mask_patch)
          cv2.imwrite(mask_weights_patch_fn, mask_weights_patch)
        util.save_obj(coords_dict, os.path.join(self.split_dir, "patch-coords"))
        plates_per_img.append(plate_patches)
      self._save_plate_stats(np.array(plates_per_img))

  def _save_plate_stats(self, plates):
    """Save some stats about the number of plates per image.

    Args:
      plates: Numpy array with the number of plate patches per image in the
        dataset.
    """
    max_p, min_p = np.max(plates), np.min(plates)
    nr_max_p, nr_min_p = np.sum(plates == max_p), np.sum(plates == min_p)
    mean_p, std_p = np.mean(plates), np.std(plates)
    stats_str = str("Plates per image stats:\n"
                    f"  max = {max_p} ({nr_max_p} times)\n"
                    f"  min = {min_p} ({nr_min_p} times)\n"
                    f"  mean = {mean_p:.2f}\n"
                    f"  std = {std_p:.2f}\n")
    zero_plates = np.nonzero(plates == 0)[0]
    for z in zero_plates:
      stats_str += f"{self.orig_fns[z]}\n"

    print(stats_str)
    with open(os.path.join(self._split_dir, "plate_stats.txt"), "w") as f:
      print(stats_str, file=f)


class Dataset:

  def __init__(self,
               dataset_split,
               patches_dir="patches",
               masks_dir="masks",
               batch_size=64):
    self._dataset_split = dataset_split
    self._patches_dir = patches_dir
    self._masks_dir = masks_dir
    self._batch_size = batch_size
    self._orig_fns = []
    self._mask_fns = []
    self._mask_weights_fns = []
    self._data = None

    # Load mean array if it's already been computed
    self._mean = None
    self._mean_fn = os.path.join(self._dataset_split.split_dir, "mean.npy")
    if os.path.isfile(self._mean_fn):
      with open(self._mean_fn, "rb") as mean_f:
        self._mean = np.load(mean_f)

    # Load standard deviation array if it's already been computed
    self._std = None
    self._std_fn = os.path.join(self._dataset_split.split_dir, "std.npy")
    if os.path.isfile(self._std_fn):
      with open(self._std_fn, "rb") as std_f:
        self._std = np.load(std_f)

  def _compute_stats(self):
    """Compute the mean and std deviation for the entire dataset."""
    dataset_mean = util.RunningMean()
    for batch in tqdm(self, desc="Computing dataset mean"):
      batch_mean = np.mean(batch, axis=0)
      dataset_mean(batch_mean, weight=len(batch))
    mean = dataset_mean.result()

    dataset_err = util.RunningMean()
    for batch in tqdm(self, desc="Computing dataset std"):
      batch_err = (batch - mean)**2
      batch_err = np.mean(batch_err, axis=0)
      dataset_err(batch_err, weight=len(batch))
    mean_err = dataset_err.result()
    scale = dataset_err.total_weight / (dataset_err.total_weight - 1)
    std = np.sqrt(scale * mean_err)

    with open(self._std_fn, "wb") as std_f:
      np.save(std_f, std)
    with open(self._mean_fn, "wb") as mean_f:
      np.save(mean_f, mean)
    return mean, std

  @property
  def std(self):
    """Return the per-pixel standard deviation for the entire dataset."""
    if self._std is None:
      self._mean, self._std = self._compute_stats()
    return self._std

  @property
  def mean(self):
    """Return the per-pixel mean for the entire dataset."""
    if self._mean is None:
      self._mean, self._std = self._compute_stats()
    return self._mean

  @property
  def orig_fns(self):
    """Return all dataset image filenames."""
    if not self._orig_fns:
      self._orig_fns = sorted(
          glob.glob(
              os.path.join(self._dataset_split.split_dir, self._patches_dir,
                           "*.png")))
    return self._orig_fns

  @property
  def mask_fns(self):
    """Return all of dataset mask filenames."""
    if not self._mask_fns:
      self._mask_fns = sorted(
          glob.glob(
              os.path.join(self._dataset_split.split_dir, self._masks_dir,
                           "*.png")))
    return self._mask_fns

  @property
  def mask_weights_fns(self):
    """Return all of dataset mask weights filenames."""
    if not self._mask_weights_fns:
      self._mask_weights_fns = sorted(
          glob.glob(
              os.path.join(self._dataset_split.split_dir,
                           f"{self._masks_dir}-weights", "*.png")))
    return self._mask_weights_fns

  def __len__(self):
    """Return the number of batches in the dataset."""
    return math.ceil(len(self.orig_fns) / self._batch_size)

  def __iter__(self):
    """Return generator that yields one Batch every call."""
    for i in range(0, len(self.orig_fns), self._batch_size):
      orig_fns = self.orig_fns[i:i + self._batch_size]
      mask_fns = self.mask_fns[i:i + self._batch_size]
      mask_weights_fns = self.mask_weights_fns[i:i + self._batch_size]
      orig_batch = []
      mask_batch = []
      mask_weights_batch = []
      for orig_fn, mask_fn, mask_weights_fn in zip(orig_fns, mask_fns,
                                                   mask_weights_fns):
        gray = cv2.imread(orig_fn, cv2.IMREAD_GRAYSCALE)
        mask = cv2.imread(mask_fn, cv2.IMREAD_GRAYSCALE)
        mask_weights = cv2.imread(mask_weights_fn, cv2.IMREAD_GRAYSCALE)
        rgb = util.gray_to_rgb(gray)
        lab = cv2.cvtColor(rgb, cv2.COLOR_RGB2LAB)
        orig_batch.append(lab)
        mask_batch.append(mask)
        mask_weights_batch.append(mask_weights)
      orig_batch_np = np.stack(orig_batch)
      mask_batch_np = np.stack(mask_batch)
      mask_weights_np = np.stack(mask_weights_batch)
      batch = util.Batch(
          orig_data=orig_batch_np,
          mask_data=mask_batch_np,
          mask_weights=mask_weights_np,
          filenames=orig_fns)
      yield batch

  def show(self):
    """Display all the images in the dataset. Press 'q' to interrupt."""
    for batch in self:
      for img, mask in zip(batch, batch.mask_data):
        h, w, c = img.shape
        frame = np.empty((h, w * 2, c))
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
        frame[:, :w] = img
        frame[:, w:] = mask
        cv2.imshow("Dataset", frame)
        key = cv2.waitKey(200)
        if key == ord('q'):
          break

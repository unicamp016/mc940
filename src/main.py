"""Main module to train and evaluate configurations."""
# pylint: disable=invalid-name, cell-var-from-loop
import json
import os

import numpy as np
from tqdm import tqdm

from src import ops, util
from src.data import Dataset, DatasetSplit, SplitType
from src.parser import parser


def main(assets_dir="assets",
         output_dir="bin",
         plate_threshold=0.5,
         classifier_type="svm",
         bands=16,
         kernel_size=3,
         patch_strategy="sliding",
         pooling="relu",
         flim=False,
         batch_norm=True,
         kernel_norm=True,
         overwrite=False,
         seed=42):
  np.random.seed(seed)

  for split in tqdm(range(1, 4), desc="Evaluating split"):
    train_split = DatasetSplit(
        split,
        SplitType.train,
        assets_dir,
        output_dir,
        plate_threshold=plate_threshold,
        overwrite=overwrite)

    if patch_strategy == "sliding":
      patch_fn = lambda orig, mask, fn: util.sliding_window_patch_gen(
          orig, mask, fn, train_split.patch_params)
    elif patch_strategy == "superpixel":
      patch_fn = lambda orig, mask, fn: util.superpixel_patch_gen(
          orig,
          mask,
          fn,
          train_split.patch_params,
          num_init_seeds=400,
          num_superpixels=80)
    else:
      raise ValueError(f"Unknown patch strategy {patch_strategy}")

    train_split.save_patches(
        patch_fn, orig_dirname="patches", mask_dirname="masks")
    train_dataset = Dataset(train_split)

    test_split = DatasetSplit(
        split,
        SplitType.test,
        assets_dir,
        output_dir,
        plate_threshold=plate_threshold,
        overwrite=overwrite)
    test_split.save_patches(
        patch_fn, orig_dirname="patches", mask_dirname="masks")
    test_dataset = Dataset(test_split)

    kernel_bank, model = None, None
    if flim:
      markers_path = os.path.join(
          assets_dir, "markers",
          f"{patch_strategy}_{int(plate_threshold * 100)}")
      model = util.load_flim_model(markers_path, bands, kernel_size)
    else:
      kernel_bank = util.load_kernel_bank(
          train_split,
          bands,
          kernel_size,
          kernel_norm,
      )
    feature_extractor = ops.FeatureExtractor(
        train_dataset,
        flim=flim,
        batch_norm=batch_norm,
        pooling=pooling,
        kernel_bank=kernel_bank,
        model=model)

    train_outputs, train_labels, _ = feature_extractor.forward_pass(
        train_dataset)
    clf = ops.Classifer(classifier_type)
    clf.fit(train_outputs, train_labels)

    test_outputs, test_labels, test_fnames = feature_extractor.forward_pass(
        test_dataset)
    print("Plotting t-SNE for extracted features...")
    util.plot_feat_space(
        test_outputs,
        test_labels,
        os.path.join(output_dir, f"split-{split}-feature-pca.pdf"),
        method="pca")
    util.plot_feat_space(
        test_outputs,
        test_labels,
        os.path.join(output_dir, f"split-{split}-feature-tsne.pdf"),
        method="tsne")
    if classifier_type == "mlp":
      print("Plotting t-SNE for last hidden layer of MLP features...")
      test_hidden_acts = clf.activations(test_outputs, len(clf) - 1)
      util.plot_feat_space(
          test_hidden_acts,
          test_labels,
          os.path.join(output_dir, f"split-{split}-hidden-pca.pdf"),
          method="pca")
      util.plot_feat_space(
          test_hidden_acts,
          test_labels,
          os.path.join(output_dir, f"split-{split}-hidden-tsne.pdf"),
          method="tsne")
    test_preds = clf.predict(test_outputs)
    csvdata = {
        "filenames": test_fnames,
        "labels": test_labels,
        "preds": test_preds
    }
    accuracy = 100 * np.sum(test_preds == test_labels) / test_preds.shape[0]
    print(f"Accuracy on split {split} is: {accuracy:.3f}")
    if classifier_type == "mlp":
      test_probs = clf.predict_prob(test_outputs)
      csvdata["probs"] = test_probs[:, 1]  # probability of being a plate
    util.write_csv(
        os.path.join(output_dir, f"split-{split}-results.csv"), csvdata)
    print(f"Finished split {split}.\n")
    # TODO: Visualize previous to last hidden layer or SVM separation.
    # TODO: Compute metrics recall, precision, AUC, F1, accuracy


if __name__ == "__main__":
  args = parser.parse_args()
  os.makedirs(args.output_dir, exist_ok=True)
  args_dict = vars(args)
  with open(os.path.join(args.output_dir, "config.json"), "w") as conf:
    conf.write(json.dumps(args_dict, indent=2))
  main(**args_dict)

\section{Evaluation}\label{sec:evaluation}

In order to determine whether a given operation contributes or not to the
results of our pipeline, we need to establish an evaluation criteria. Even
though the final task is to choose a patch with the highest intersection with
the plate, we focused on improving the performance of the classifier
and only in Section~\ref{sec:postproc} concern ourselves with the
post-processing step of selecting the best patch among the ones classified as
plates.

In previous tasks, we evaluated the contribution of different operations by
ranking the quality of the activations in the multi-band image output by the
feature extractor. To do so, we used weighted $L^1$ and $L^2$ norms between the
output feature map and the plate annotation mask. It will be interesting to see
how effective this criteria was now that we have the full pipeline with the
classifier.

The first two components of the pipeline are the feature extractor and the
classifer. The feature extractor outputs a multi-band image with the same
height and width as the RGB input image. Throughout all experiments we used 16
kernels in the convolutional feature extractor to ensure that the output
features had the same dimensions and were directly comparable with one another.
Before feeding the classifier, we aggregate the features using max-pooling with
a $2 \times 2$ kernel and $2 \times 2$ strides to reduce its dimensionality.
Then, we flatten the feature vector and use either a Multi-Layer Perceptron
(MLP) or a Support Vector Machine (SVM) and fit the classifier on the patches
from a given training split before evaluating the learned model on the
respective test split. We use three random splits of the \num{200} image
dataset, each with \num{100} training images and \num{100} test images, to
compute statistics for the evaluation metrics. All metrics are reported as the
mean value on the three splits together with the \SI{95}{\percent} confidence
interval of the value estimate.

One of the advantages of using a MLP with a sigmoid activation in the
last hidden layer over a SVM is that we have access to the probability of a
patch being a plate and can easily visualize the output activations of hidden
layers.  The probabilities allow us to compute metrics like the AUC and plot
the ROC curve for a more robust quantitative evaluation, while also enabling us
to use a dimensionality reduction algorithm, like t-SNE, to qualitatively
evaluate how the feature space changes from the input, to the output of the
feature extractor to the output of the last hidden layer of the MLP. Therefore,
we chose to use a MLP classifier throughout the experiments and evaluated the
difference in performance with the SVM in Section~\ref{sec:classifier}. Details
about the parameters used to train the MLP and the SVM are also avialable in
this section.

When selecting the best model we must keep in mind that our patch dataset is
unbalanced with approximately \SI{85}{\percent} of split 1 comprised of
background images. Therefore, accuracy isn't the best peformance metric since a
high accuracy can be achieved with a dummy classifier that that classifies all
patches as background. For a quantitative evaluation, we measure five
different metrics: accuracy, precision, recall, F1 score, and the AUC. For a
qualitative evaluation, we project the input feature space, the features
extracted from the convolutional and pooling layers, and the last hidden layer
of the MLP. We chose to use the AUC as the deciding metric to select the best
pipeline configuration.

\section{Post-processing}\label{sec:postproc}

The post-processing step is paramount to evaluate the performance of the
pipeline in the desired task: to select a patch with the highest possible
intersection with a plate. To select the best candidate to have the highest plate
intersection, we take the average between the center coordinates of every patch
classified as a plate and choose the patch who's center is closest to this
coordinate.

The patch extraction strategy used is crucial for this task, since it
determines the quality of our final plate center estimate. We already discussed
the sliding window strategy in Section~\ref{sec:sliding}. In
Section~\ref{sec:postproc-sup}, we present a new strategy based on superpixels.
In Section~\ref{sec:plate-estimate}, we compare the sliding window approach
with the superpixel one in estimating the center of the plate.

\subsection{Superpixel}\label{sec:postproc-sup}

Superpixels are groups of connected pixels that share similar characteristics
according to a predicate. Dynamic Iterative Spanning Forest
(DISF)~\cite{belem2020} is a path-based superpixel segmentation method that
adopts the classic three-step pipeline to extract superpixels: (i) initial seed
sampling; (ii) superpixel delineation; and (iii) seed recomputation. Compared to
other seed-based superpixel methods, DISF is more likely to find relevant seeds,
since it starts from a considerably oversampled number of seeds than the desired
number of superpixels. Due to its dynamic arc-weight estimation, DISF adapts the
path-cost function to consider mid-level image properties with each growing
tree, resulting in a more reliable estimation in the cost to incorporate a new
pixel.

The invocation of DISF is parameterized by the number of initial seeds $N_0$,
number of final seeds $N_f$ and adjacency relation $\mathcal{A}$, which defines
the pixels that are considered when extending a path $\pi_s$ by an arc $(s, t)$
in the Image Foresting Transform (IFT)~\cite{falcao2004}. To extract the
patches, we computed the geometric center $(x_l, y_l)$ of each superpixel $\tau
= \{p \in D_I | L(p) = l\}$ with label $l \in [1, N_f]$ as

\begin{align}
x_l = \frac{1}{|\tau|} \sum_{p \in D_I | L(p) = l} x_p, \\
y_l = \frac{1}{|\tau|} \sum_{p \in D_I | L(p) = l} y_p.
\end{align}

One patch with dimensions $W_p \times H_p$ centered in $(x_l, y_l)$ was
extracted for each superpixel, where we discarded the patches that extrapolated
the image boundaries.

Following the same approach used in Section~\ref{sec:sliding} to determine the
hyperparameters, we found that using $N_0 = \Ni$, $N_f = \Nf$, and $\mathcal{A}:
{(p, q) \in D_I \times D_I \, | \, \| q - p \|_2 \le \Asupxl}$ netted the good
results for the training split, with $p_{\min} = \num{2}$ (\SI{1}{\percent} of
the time), $p_{\max} = \num{15}$ (\SI{1}{\percent} of the time), and $p_{\mu} =
\num{7.85}$. These results were similar for the test split, where $p_{\min} =
\num{1}$ (\SI{1}{\percent} of the time) and
$p_\mu = \num{7.60}$. Figure~\ref{fig:disf} shows the samples in which DISF
didn't output a superpixel centered in a plate. In Figure~\ref{fig:disf-a} we
can see some examples of the superpixels computed for images in test set 1.
Note that even though we specify a final number of seeds of $N_f = 80$, we end
up with less than half of the seeds as patches, since we discard the ones
that extrapolate boundaries of the image.

Notice how the superpixel approach helps us ignore homogeneous regions in the
background and doesn't extract excessive patches that don't contribute to the
task. In the bottom-right corner of Figure~\ref{fig:disf-a}, we can see that
the entire region is reduced to 2 superpixels, which allows more patches to be
extracted around the plate, as we can see from Figure~\ref{fig:disf-d}
and~\ref{fig:disf-e}. With more patches around the plate, we can make a better
estimate of the plate's center coordinates, which is ultimately the goal of our
pipeline. Because superpixels selects more relevant patches, we expect it to be
able to improve the sliding window approach using a smaller dataset. For
instance, in training split 1, the patch dataset extracted with superpixels was
composed of 3611 images, which is approximately \SI{10}{\percent} less than the
sliding window dataset.  The superpixel approach also allowed us to mitigate
the effect of an unbalanced dataset, by increasing the percentage of plate
patches to \SI{21.7}{\percent}, as opposed to the \SI{15.2}{\percent} of the
sliding
window approach.

\begin{figure}
    \centering
    \begin{subfigure}{0.32\textwidth}
      \includegraphics[width=\textwidth]{./images/supxl/orig_0030.png}
      \caption{}
      \label{fig:disf-a}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.32\textwidth}
      \includegraphics[width=\textwidth]{./images/supxl/orig_0071.png}
      \caption{}
      \label{fig:disf-b}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.32\textwidth}
      \includegraphics[width=\textwidth]{./images/supxl/orig_0125.png}
      \caption{}
      \label{fig:disf-c}
    \end{subfigure} \\
    \begin{subfigure}{0.32\textwidth}
      \includegraphics[width=\textwidth]{./images/supxl/orig_0146.png}
      \caption{}
      \label{fig:disf-d}
    \end{subfigure}
    \hspace{0.5em}
    \begin{subfigure}{0.32\textwidth}
      \includegraphics[width=\textwidth]{./images/supxl/orig_0186.png}
      \caption{}
      \label{fig:disf-e}
    \end{subfigure}
    \caption{Sample images from test split 1 where we applied
    DISF~\cite{belem2020} to select superpixels.  A different color is assigned
    to each superpixel. The red circles in each image are the geometric centers
    of each superpixel. The magenta bounding box represents the extracted patch
    with dimensions $W_p \times H_p$. The superpixels
    without a bounding box were discarded when extracting the patches since the
    patch would extrapolate the image boundaries.}
    \label{fig:disf}
\end{figure}

\subsection{Plate Center Estimation}\label{sec:plate-estimate}

We can't use accuracy or any of the classification metrics used in
previous sections to compare both patch extraction strategies, since they change
the underlying dataset used to train and evaluate the pipeline. Therefore,
we devised a new metric that's concerned with the final task of our pipeline,
estimating the coordinatees of the center of the plate. Given a set of $j$
patches $P_i \in \mathcal{P}$ for an image $I$ belonging to the dataset
$\mathcal{D}$ with center coordinates $(c_{x_{ij}}, c_{y_{ij}})$ and labels
$\lambda_{ij}$. We can estimate the center of the plate $(p_{x_i}, p_{y_i})$
for image $I$ by computing

\begin{align}
  p_{x_i} &= \frac{1}{|\lambda_i|} \sum_{j} \lambda_{ij} c_{x_{ij}},  \\
  p_{y_i} &= \frac{1}{|\lambda_i|} \sum_{j} \lambda_{ij} c_{y_{ij}},
\end{align}

where $|\lambda_i|$ is the number of patches labeled as plates in image $i$.
With our estimate of the plate center coordinates, we can extract a patch
from the mask with dimensions $W_p \times H_p$. Then, we can measure the number
of pixels of the plate within our extracted patch $n_{p_i}$ divided by the number of
plate pixels in the mask of the entire image $n_{m_i}$. This gives us

\begin{equation}\label{eq:rplate}
  r_{plate} = \frac{1}{|\mathcal{D}|} \sum_i \frac{n_{p_i}}{n_{m_i}} \times \SI{100}{\percent},
\end{equation}

the mean percentage of the plate pixels that fall within the patch extracted from
our estimate of the plate center coordinate.

Figure~\ref{fig:postproc} shows some examples of estimated plate center coordinates
for each patch extraction strategy. Note how the superpixel approach
has numerous positive patches surrounding the plate, allowing to make a better
estimate of its center. On the other hand, the quality of the estimate with the
sliding window approach is highly dependent on the displacement $D_x$ and $D_y$
between patches, making unnecessarily increase the number of background images
and the size of the entire patch dataset in order to improve the estimate
of the plate center. This highlights another advantage of the superpixel approach,
which is able to get better plate estimates even with a smaller dataset, since
it doesn't extract multiple patches for homogeneous regions in the background,
which aren't beneficial for the pipeline's performance.

\begin{figure}
  \centering
  \begin{subfigure}{\textwidth}
    \centering
    \begin{subfigure}{0.33\textwidth}
      \includegraphics[width=\textwidth]{images/postproc/sliding/0019.png}
    \end{subfigure}
    \begin{subfigure}{0.33\textwidth}
      \includegraphics[width=\textwidth]{images/postproc/sliding/0027.png}
    \end{subfigure}
    \caption{Sliding Window}\label{fig:postproc-sw}
  \end{subfigure}
  \begin{subfigure}{\textwidth}
    \centering
    \begin{subfigure}{0.33\textwidth}
      \includegraphics[width=\textwidth]{images/postproc/supxl/0019.png}
    \end{subfigure}
    \begin{subfigure}{0.33\textwidth}
      \includegraphics[width=\textwidth]{images/postproc/supxl/0027.png}
    \end{subfigure}
    \caption{Superpixels}\label{fig:postproc-sup}
  \end{subfigure}
  \caption{Sample images from test set 3 with the coordinates of the patches
  classified as plates (blue) and the coordinates of our estimate of the center
  of the plate (orange) for the sliding window~\subref{fig:postproc-sw} and
  superpixel~\subref{fig:postproc-sup} patch extraction
  strategies.}\label{fig:postproc}
\end{figure}

We used $r_{plate}$ and the size of the patch dataset $|\mathcal{P}|$ to
evaluate the performance of both patch extraction strategies.
Table~\ref{tab:postproc} shows the results of the metrics for each strategy.
The superpixel outperformed the sliding window approach both in making good
estimates of the patch center coordinates and in reducing the size of the
dataset needed to do so. We were able to improve $r_{plate}$ by approximately
\SI{1}{\percent} while reducing the size of the patch dataset by
\SI{10}{\percent}.

\begin{table}
\centering
\begin{tabular}{|l|c|c|}
  \hline
  Pipeline        & $r_{plate}$ (\%)             & $|\mathcal{P}|$ \\\hline
  Sliding Window  & $97.426 \pm 0.501$           & $4000 \pm 0$ \\
  Superpixel      & $\mathbf{98.349 \pm 0.448}$  & $\mathbf{3605 \pm 89}$ \\
  \hline
\end{tabular}
  \caption{Mean with \SI{95}{\percent} confidence interval of $r_{plate}$, our
  custom evaluation metric defined in Equation~\ref{eq:rplate}, and
  $|\mathcal{P}|$, our patch dataset size. Both pipelines used batch
  normalization, FLIM as the feature extractor, and max-pooling with kernel
  size $7 \times 7$. The best performing patch extraction strategy according to
  our metric is the superpixel approach.}
\label{tab:postproc}
\end{table}

\section{Normalization}\label{sec:normalization}

% todo: maybe talk about normalization

\subsection{Batch Normalization}\label{sec:batch-norm}
Batch normalization can be applied to the entire dataset to change the values of
the random variables to a common scale. To do so, we apply the $z$-score
normalization, in which we compute the mean and standard deviation value for
every pixel position between all images in the training split. Then, for every
input image, we subtract the computed mean and divide the result
by the standard deviation. This ensures that all pixels have a similar
distribution. Figure~\ref{fig:norm} shows the mean and standard deviation images
computed from training split 1. Note that the bottom left corner of the mean
image in Figure~\ref{fig:norm-mean} has a darker shade of blue than the rest of
the image, meaning that this region had generally darker shades of gray than the
rest. In Figure~\ref{fig:norm-std}, we can see that the lower left region of the
images have lower variance than the upper part, due to the darker shade of
green.

\begin{figure}[h]
  \centering
  \begin{subfigure}{0.35\textwidth}
    \includegraphics[width=\textwidth]{./images/task-1/batch-norm/mean.png}
    \caption{Mean}
    \label{fig:norm-mean}
  \end{subfigure}
  \begin{subfigure}{0.35\textwidth}
    \includegraphics[width=\textwidth]{./images/task-1/batch-norm/std.png}
    \caption{Standard Deviation}
    \label{fig:norm-std}
  \end{subfigure}
  \caption{Mean~\subref{fig:norm-mean} and standard
  deviation~\subref{fig:norm-std} between all colorized images of training split
  1. Due to the darker shade of blue and green in the lower left corner of the
  image, we can see that it is a region of lower variance and generally lower
  pixel grayscale values.}
  \label{fig:norm}
\end{figure}

Using the evaluation criteria defined in Section~\ref{sec:evaluation}, we
compared the performance of the pipeline with and without batch normalization,
experimenting with $3 \times 3$ and $5 \times 5$ kernels. The random kernel
weights were sampled from a continuous uniform distribution
over the half-open interval [0, 1). Table~\ref{tab:batch-norm}, shows
the performance of each configuration. For the pipeline without batch
normalization and kernel size $3 \times 3$, the classifier wasn't able to converge
to a non-trivial solution consistently, often getting stuck on predicting all
samples as plates or background depending on the train and test splits. This
is reflected on the wide confidence interval, which has the same order of
magnitude as the mean accuracy. Increasing the kernel size to $5 \times 5$
was not hepful, since the classifier still had a hard time learning a non-trivial
solution, which is reflected by the low F-score with a wide confidence interval.
This example also showcases how accuracy isn't a good metric to make decisions
since we were still able to acheive a relatively high mean accuracy despite
learning trivial solutions. From the AUC, we can see how the pipelines without
batch normalization were only slightly better than a uniformly random classifer,
which predicts a sample as being positive or negative with a \SI{50}{\percent}
chance.

After introducing batch normalization, there was a considerable improvement in
the classifier's performance. Learning was stable across all three splits,
which we can see from the narrow confidence intervals across all metrics. The
classifier always converged to a non-trivial solution achieving a good balance
between precision and recall and considerable imporving the AUC. Note that using
$5 \times 5$ kernels did not contribute to the classifier's performance.

\input{results/batch-norm/table}

Figure~\ref{fig:bn-roc} shows the ROC curves for all batch normalization
experiments. Note how without batch normalization the model the variance is
extremely high, but is generally close the baseline curve of a random
classifier. With batch normalization, there is a significant performance
improvement as we can see from the steepness of the curve. We also found that
using $3 \times 3$ kernels contributed to reduce the variance in the
classifier's performance relative to the $5 \times 5$ kernels.

\input{results/batch-norm/roc}

Throughout all experiments, we also observe the activations output by the
convolutional feature extractor on the sample image from
Figure~\ref{fig:sample-image}.

\begin{figure}[h]
  \centering
  \begin{subfigure}{0.35\textwidth}
    \includegraphics[width=\textwidth]{./images/1_orig_0194_0032.png}
    \caption{Original}
    \label{fig:exp-orig}
  \end{subfigure}
  \begin{subfigure}{0.35\textwidth}
    \includegraphics[width=\textwidth]{./images/1_mask_0194_0032.png}
    \caption{Mask}
    \label{fig:exp-mask}
  \end{subfigure} \\
    \caption{Sample foreground patch from test split 1 with
    original~\subref{fig:exp-orig} and mask~\subref{fig:exp-mask} frames used to
    showcase ReLU's output in all experiments.}
  \label{fig:sample-image}
\end{figure}

Figures~\ref{fig:no-bn-3x3}
to~\ref{fig:with-bn-5x5} show the outputs of the convolutional feature
extractor for each of the pipeline configurations in the batch normalizaton
experiments. These activations highlight the reason that batch normalization is
so important for learning. Figures~\ref{fig:no-bn-3x3} and~\ref{fig:no-bn-5x5}
show the outputs for the pipelines without batch normalization and kernel sizes
$3 \times 3$ and $5 \times 5$, respectively.  Without batch normaliation, the
activations for most kernels are positive and the ReLU can't filter out
relevant regions from the patch. Using a $5 \times 5$ kernel only adds a
smoothing effect to the output, but the underlying issue with high activations
is still present. Figures~\ref{fig:with-bn-3x3} and~\ref{fig:with-bn-5x5} show
the outputs for the pipelines with batch normalization and kernel sizes $3
\times 3$ and $5 \times 5$, respectively.  With batch normalization we can see
how some kernels are able to effectively highlight the plate, distributing the
activations around 0 in order for the ReLU to filter out large portions of the
image. Note how kernels play distinct roles, some highlighting the edges,
others the plate, and others the background around the plate. This variety of
features helps the classifier distinguish between plates and background.
However, we can also observe how there is a lot of redundancy in the filters,
given that several of them highlight the same regions of the plate. Using a $5
\times 5$ kernel only adds a smoothing effect to the output, which doesn't seem
to benefit the classifier, most likely because we some of the granularity of
our features, make some of the filter outputs even more simliar to one another.

\input{results/batch-norm/figs}

Finally, we qualitatively evaluate the result by observing how the projection
of the intermediate outputs of our pipeline change for each configuration.
Figure~\ref{fig:proj-batch-norm} shows the projection of the input feature
space, the output of the convolutional feature extractor, and the last hidden
layer for all batch normalization experiments using PCA. From the middle
column we can see how batch normalization does a significantly better job
of separating the plate samples from the background ones in the leftmost
corner of the projection, making the classifier's job much easier and resulting
a clear separation at output of the last hidden layer.

\input{results/batch-norm/tsne}


\subsection{Kernel Normalization}\label{sec:kernels}

Normalizing the weights of the kernels is a way of ensuring that all kernel
contribute evenly to the output activations. Subtracting the mean of the kernels
ensures that the activations will be better distributed around zero, so that
the ReLU can do a better job of filtering certain regions.

Table~\ref{tab:kernel-norm} shows the metric results from the pipeline with
batch normalization and kernels sampled from a uniform distribution (With-BN-3x3)
from the previous section, along with results of applying $z$-score kernel and batch
normalization using kernels with sizes $3 \times 3$ and $5 \times 5$.
After applying $z$-score normalization to the kernels, the metric results
showed a slight improvement with respect to the pipeline only with batch
normalization. Note how the there was a direct improvement in the AUC when
adding kernel normalization, but only for the same kernel sizes. The pipeline
without kernel normalization and $3 \times 3$ kernels still performed better
than the one with and $5 \times 5$ kernels, suggesting that the smoothing effect
from larger kernels is in fact harmful for the classifier. Still, we were able
to see an improvement of about \SI{0.4}{\percent} on our mean estimate of the
AUC relative to the experiment with only batch normalization, despite both
estimates being within the \SI{95}{\percent} confidence interval.

\input{results/kernel-norm/table}

Figure~\ref{fig:kn-roc} shows the ROC curves for the three configurations in
Table~\ref{tab:kernel-norm}. Note how most curves are within the confidence
interval of one another, but our mean estimate for the $3 \times 3$ kernel
normalization experiment is slightly steeper than the rest, making it the best
choice for the remaining experiments.

\input{results/kernel-norm/roc}

Figures~\ref{fig:kn-3x3} and~\ref{fig:kn-5x5} show the output activations of
our feature extractor for the pipelines with kernel normalizaton and kernel
sizes $3 \times 3$ and $5 \times 5$, respectively. Note how kernel normalization
does a better job of distributing the types of filters in our convolutional
layer. While Figure~\ref{fig:with-bn-3x3} showed several redundant features
that highlight the plate, Figure~\ref{fig:kn-3x3} shows an even distribution
of filters that enhance edges, filters that enhance the background of the plate,
and filters that enhance the plate itself. With less redundancy, the classifier
has more information to work with and can do a better job of separating plate
from background patches. Figure~\ref{fig:kn-5x5} shows that using larger
kernels smoothens out the features, which isn't beneficial for our already
low-resolution patches.

\input{results/kernel-norm/figs}

Figure~\ref{fig:proj-kernel-norm} shows the projections of relevant pipeline
steps for the kernel normalization experiments using PCA. Note how the output
of the feature extractor isn't as concentrated in a particular region of the
projection as it was in Figure~\ref{fig:proj-batch-norm}. Instead, the feature
extractor contributes to making the feature space sparser, with plate
points further apart from neighboring background points, making the
classifier's job easier.

\input{results/kernel-norm/tsne}

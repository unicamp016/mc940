\section{FLIM}\label{sec:flim}

Feature Learning from Image Markers (FLIM)~\cite{flim} is a method used to find
a set $\mathcal{F}$ of filters that enhance discriminanting regions from all
classes. The user selects a few images from a small dataset $\mathcal{D}$ and
draws labeled markers in image regions that best discriminate  the classes.
The method estimates the weights of the filters $F \in \mathcal{F}$ such that
the local patterns of the discriminanting regions are enhanced. We wish to
study how this set of filters contributes to our task of discriminating plates
from background patches. Section~\ref{sec:flim-marker} outlines the method we
used to select the markers used by FLIM to learn the set $\mathcal{F}$.
Section~\ref{sec:flim-features} replaces our set of random kernels by
$\mathcal{F}$ and compares the performance of the new pipeline using the
criteria outlined in Section~\ref{sec:evaluation}.

\subsection{Marker Selection}\label{sec:flim-marker}
To draw the markers that best discriminate regions from each class we follow
the suggested pipeline by~\citet{flim}. First, we project the images from
the traning set using t-SNE, as seen in Figure~\ref{fig:tsne-proj}, and select
two points for each class of images that come from regions in the projection
populated by samples from the same class. These regions are groups of images
from the same class in the input feature space, and the markers drawn on them
are more likely to generalize for other images from the same class.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\textwidth]{images/flim/split-1-perp-50.pdf}
  \caption{Input feature space for traning split 1 projected using t-SNE with
  perplexity 50. The green points are images of the plate, the red points are
  images of the background, and the orange points are the images selected to
  draw markers on due to their proximity to other images of the same class in
  the projection}
  \label{fig:tsne-proj}
\end{figure}

After selecting four images from the projection in Figure~\ref{fig:tsne-proj},
two from each class, we drew markers in the regions that best distinguish plates
from the background. Figure~\ref{fig:markers} shows the markers drawn on each
of the four images, where we chose to highlight the characters of the plate as
the main feature that distinguishes plates from their background.

\begin{figure}[h]
  \centering
  \begin{subfigure}{\textwidth}
    \centering
    \begin{subfigure}{0.4\textwidth}
      \includegraphics[width=\textwidth]{images/markers/sliding_50/0_orig_0025_0008.png}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
      \includegraphics[width=\textwidth]{images/markers/sliding_50/0_orig_0085_0006.png}
    \end{subfigure}
    \caption{Background}
    \label{fig:markers-back}
  \end{subfigure}\vspace{2px}
  \begin{subfigure}{\textwidth}
    \centering
    \begin{subfigure}{0.4\textwidth}
      \includegraphics[width=\textwidth]{images/markers/sliding_50/1_orig_0028_0025.png}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
      \includegraphics[width=\textwidth]{images/markers/sliding_50/1_orig_0194_0032.png}
    \end{subfigure}
    \caption{Plate}
    \label{fig:markers-plate}
  \end{subfigure}
  \caption{Image markers drawn on the images selected from the projection in
  Figure~\ref{fig:tsne-proj}. The top row are the two background images, where
  we drew orange markers in the regions that characterize the background. The
  bottom row are two plate images, where we drew blue markers in the characters
  of the plate and orange markers in regions surrounding the plate.}
  \label{fig:markers}
\end{figure}

\subsection{Feature Extraction}\label{sec:flim-features}

To compute the filters $F$, we need to first define an architecture so that
FLIM can, for every layer, select the filters that enhance the marked regions.
We chose a simple architecture in order to compare FLIM to the other feature
extraction methods studied in the previous sections. The architecture consists
of \num{3} layers. First, a convolutional layer that preserves the input
dimensions with kernel shape $3 \times 3$, ReLU activation, and \num{16} output
bands, \num{8} for each marked class. Then, a pooling layer with kernel $3
\times 3$ that also preserves the input dimensions followed by a batch
normalization layer. This CNN can be used in place of our random kernels from
Section~\ref{sec:normalization}, and we can feed its features to the pooling
operations studied in Section~\ref{sec:pooling} before finally classifying the
patches with a MLP.

Figure~\ref{fig:flim-plain} shows the activations output by
FLIM without any of the pooling operations discussed in
Section~\ref{sec:pooling}. Note how FLIM does a good job of separating the
plate pixels from their background, either by enhancing the characters or the
entire plate. By design, FLIM selects the same number of filters to enhance
the features from each class. This ensures that we won't fall into a common
pitfall with random weights of selecting redundant filters that focus too much
on enhancing only one of the classes in our problem.

Figure~\ref{fig:flim-maxpool} shows the output activations of the FLIM feature
extractor after applying a max-pooling with kernel $7 \times 7$, the best-performing
pooling operation from Section~\ref{sec:pooling}. Note how the plates become
homogeneous blocks in the output activations, making it clearer for the classifier
when there's a plate in the input image.


Table~\ref{tab:flim} shows the computed metrics for two FLIM pipelines
alongside the best-performing pipeline that uses random kernels. Note just by
using FLIM, without using any form of pooling, we were able to improve the
model's accuracy by approximately \SI{2}{\percent}. For the metrics that
account for the unbalanced dataset, we saw an improvement of \SI{7.9}{\percent}
in the F-score and \SI{0.7}{\percent} in the AUC. Figure~\ref{fig:flim-roc}
shows the ROC curves for the experiments in Table~\ref{tab:flim}. Note how the
curves of the experiments with FLIM aren't even within the \SI{95}{\percent}
confidence interval of the curve without, showing that there was undoubtedly a
performance improvement.  Using max-pooling with kernel $7 \times 7$ further
improved the performance of the classifer, although very slightly with respect
to the pipeline without any pooling. We chose to use this last pipeline for the
remaining experiments, since it achieves the best performance in the AUC.

\input{results/flim/table}
\input{results/flim/roc}
\input{results/flim/figs}

Figure~\ref{fig:proj-flim} shows the PCA projection for both FLIM experiments.
We can see that with FLIM, the projections of the output of the feature
extractor are even sparser, further contributing to the performance of the
classifier, which has an easier time separating plate from background patches.

\input{results/flim/tsne}

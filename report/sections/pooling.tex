\section{Pooling}\label{sec:pooling}

Pooling operations are a way to further enhance activations, aggregating
features with a reduction operation within an adjacency relation $\mathcal{A}$.
In this section we will explore the effect of applying pooling operations after
the ReLU activation $\mathrm{\mathbf{R}}$ to create images $\mathrm{\mathbf{P}}$.
Sections~\ref{sec:maxpooling} and~\ref{sec:avgpooling} detail the results of
applying max-pooling and average pooling, while Section~\ref{sec:haar} outlines
the use of Haar-like features to enhance the location of the plate.

Table~\ref{tab:pooling} shows the metric results for all the experiments
with pooling operations, including the best result obtained without any
pooling (With-KN-3x3). Throughout this section, we will refer to this table
to discuss the results.

\input{results/pooling/table}

Figure~\ref{fig:pooling-roc} shows the ROC curves for all experiments in
Table~\ref{tab:pooling}, which we will also be referring to throughout this
section.

\input{results/pooling/roc}

\subsection{Max-Pooling}\label{sec:maxpooling}

Max-pooling is a pooling operation defined by

\begin{equation}
  P_i(p) = \max_{q \in \mathcal{A}(p)} R_i(q),
\end{equation}

for $i \in \{1, 2, ..., b\}$, where $b$ is the number of activation bands. In
machine learning algorithms, it is typically used to reduce the dimensionality
of the features for further processing and introduce a small form of translation
invariance to the learned features. In this definition, the operator is computed
for every pixel and the bands $P_i$ have the same dimension as $R_i$.

We evaluated the performance of the operator with two different rectangular
adjacency relations: $\mathcal{A}_{3\times3}$ with kernel size $3\times3$ and
$\mathcal{A}_{7\times7}$ with size $7\times7$. Table~\ref{tab:pooling} shows
the performance of each adjacency relation using the evaluation criteria
defined in Section~\ref{sec:evaluation}. We found that for
$\mathcal{A}_{3\times3}$, the pipeline had a worse performance than without any
pooling. While for $\mathcal{A}_{7\times7}$, there was a significant performance
increase, achieving the best performance among the other pooling operations in
both accuracy and the AUC.

Figures~\ref{fig:max-3x3} and~\ref{fig:max-7x7} show the
activations for a sample plate image obtained with $\mathcal{A}_{3\times3}$ and
$\mathcal{A}_{7\times7}$, respectively. The figures show how using a larger
adjacency relation reduces the amount of detail in the image, making the
caracters of the plate almost unrecognizable in some of the activations.

\input{results/pooling/maxpool/figs}

In Section~\ref{sec:normalization} we generally observed the opposite pattern,
where increasing the sizes of the kernels was not beneficial for the classifier
and the $3 \times 3$ kernels consistently outperformed the $5 \times 5$ ones.
However, we must note that max-pooling has a different effect than increasing
filter kernel sizes, since instead of smoothing the activations, it sharpens
them, removing unnecessary details from the image while still maintaining the
magnitude of the activations. We imagine that this effect is beneficial
for the classifer, which has an easier job of distinguishing between features
with a higher discrepancy in their activaton values.

Figure~\ref{fig:proj-maxpool} shows the PCA projections of the relevant steps
in the pipeline for both max-pooling experiments. We can see how the classifier
is able to separate the plate samples even further apart from the background
ones when using the $7 \times 7$ kernel.

\input{results/pooling/maxpool/tsne}

\subsection{Average Pooling}\label{sec:avgpooling}

Similarly to max-pooling, we can define average pooling as

\begin{equation}
  P_i(p) = \frac{1}{|\mathcal{A}(p)|} \sum_{q \in \mathcal{A}(p)} R_i(q).
\end{equation}

To evaluate this operator, we used the same adjacency relations
$\mathcal{A}_{3\times3}$ and $\mathcal{A}_{7\times7}$ from
Section~\ref{sec:maxpooling}. Table~\ref{tab:pooling} displays the results
obtained with average pooling for each of the adjacency relations. From the
results we can see that both average pooling operations outperformed the
pipeline without any pooling, but were still worse than max-pooling with
$\mathcal{A}_{7\times7}$. Again we observed the same pattern with the previous
pooling operations, where the larger adjacency relations benefit the classifier.

Figure~\ref{fig:avg-3x3} and~\ref{fig:avg-7x7} show the activations for a
sample plate image obtained with $\mathcal{A}_{3\times3}$ and
$\mathcal{A}_{7\times7}$, respectively. Note how average pooling has a similar
effect to increasing the filter kernel sizes, since it smoothens the
output activations. Even though this effect somewhat benefits the classifier by
aggregating features and mitigating redundant information, it doesn't do so in
a manner as assertive as max-pooling, which doesn't leave several intermediate
activations throughout the image. We believe that the lower contrast between
plate activations and background ones contribute to the worse performance of
average pooling relative to max-pooling. Also, while max-pooling was able to
enhance small regions of positive activations in the characters of the plate,
average pooling plays the opposite role of mitigating these activations and
further darkening the plate regions.

\input{results/pooling/avgpool/figs}

Figure~\ref{fig:proj-avgpool} shows the PCA projections for both average
pooling experiments. We can somewhat see how the plate samples are more coupled
with the background ones, which doesn't benefit the classifier.

\input{results/pooling/avgpool/tsne}

\subsection{Haar-like Features}\label{sec:haar}

Haar-like features were introduced by Viola \emph{et al.}~\cite{viola2004} in
their work with a real-time face detection pipeline. Haar-like features are
computed from the weighted sums of all pixels in a set of regions that make up a
feature type. Figure~\ref{fig:haar-types} shows the different feature types used
throughout the experiments, where the black regions have negative weights and
the white regions have positive weights. This means that the filters enhance
pixels that have large activations in the white region and low activations in
dark regions. Since we computed per-pixel haar-like features, we had to zero pad
the ReLU activations $\mathrm{\mathbf{R}}$ to ensure that we could compute the
features for the pixels in the boundaries of the image.

\begin{figure}[h]
    \centering
    \begin{subfigure}{0.23\textwidth}
      \includegraphics[width=\textwidth]{./images/haar-types/type-3-x-3x3.pdf}
      \caption{$H_{3x_{3\times3}}$}
      \label{fig:haar-3x}
    \end{subfigure}
    \hspace{1em}
    \begin{subfigure}{0.23\textwidth}
      \includegraphics[width=\textwidth]{./images/haar-types/type-3-y-3x3.pdf}
      \caption{$H_{3y_{3\times3}}$}
      \label{fig:haar-3y}
    \end{subfigure}
    \begin{subfigure}{0.23\textwidth}
      \includegraphics[width=\textwidth]{./images/haar-types/plate-35x35.pdf}
      \caption{$H_{p_{35\times35}}$}
      \label{fig:haar-plate-35x35}
    \end{subfigure}
    \begin{subfigure}{0.23\textwidth}
      \includegraphics[width=\textwidth]{./images/haar-types/plate-35x70.pdf}
      \caption{$H_{p_{35\times70}}$}
      \label{fig:haar-plate-35x70}
    \end{subfigure}
    \caption{Haar-like feature types used in the experiments. Feature
    types~\subref{fig:haar-3x} and~\subref{fig:haar-3y} are borrowed from the
    Viola \emph{et al.}'s work with face detection~\cite{viola2004}, where we
    attributed weights \num{-1} and \num{2} to the black and white regions,
    respectively. Feature types~\subref{fig:haar-plate-35x35}
    and~\subref{fig:haar-plate-35x70} were created specifically for plate
    enhancement, where we used weights \num{-1} for all black regions and
    weight \num{4} for the white region.}
    \label{fig:haar-types}
\end{figure}

Table~\ref{tab:pooling} shows the metrics for the four feature types we
experimented with. Note how all feature types achieved a worse performance than
the pipeline without any pooling for the AUC, despite $H_{p_{35\times35}}$
achieving a slightly higher accuracy and F1-score. The two feature types
$H_{3x_{3\times3}}$ and $H_{3y_{3\times3}}$ borrowed from Viola \emph{et al.}
had a poor overall performance, with $H_{3y_{3\times3}}$ achieving a worse performance
than the initial pipelines with only batch normalization.
Figure~\ref{fig:pooling-roc} shows how both custom-designed feature types had a
hard time converging consistently, and sometimes got stuck in local optima
classifying all patches as background. This is somewhat justified, considering
the large dimensions of the filter, which are meant to resemble a plate, but
end up making most of the output activations too similar to one another.

Figures~\ref{fig:haar-3x-acts} to~\ref{fig:haar-35x70-acts} show sample output
activations for each of the Haar feature types. Figure~\ref{fig:haar-3x-acts}
shows how $H_{3x_{3\times3}}$ behaves like a horizontal gradient filter. The
problem of using this as a pooling operator is that it ends up undoing the work
that the convolutional filters did to extract different features from the
image.  The Haar feature makes it so all activations are somewhat homogeneous,
which defeats the purpose of using random kernels that enhance different
regions of the plate. Similarly, Figure~\ref{fig:haar-3y-acts} shows how
$H_{3y_{3\times3}}$ behaves like a vertical gradient filter, and suffers from
the same problems of $H_{3x_{3\times3}}$, where all output activations are
similar and the classifier has a hard time distinguishing between plates and
background images.

Figure~\ref{fig:haar-35x35-acts} shows the output activations for
$H_{p_{35\times35}}$.  Note how this time there are mainly two classes of
activations, the ones that have a white smudge in the region of the plate and
the negative counterparts of those, which have high activations for all regions
except the plate. Despite this filter being slightly better than the two
borrowed from Viola \emph{et al.}, it still lacks enough diversity in the
output activations to help the classifier distinguish plates from non-plates.
Figure~\ref{fig:haar-35x70-acts} shows a similar behavior for
$H_{p_{35\times70}}$, where we used a larger windows to completely capture the
plate. The effect of separating the output activations into two classes was
similar, and this explains some of the inconsistent behavior in training, where
the classifier sometimes had a hard time learning a non-trivial solution.

By inspecting these output activations, we noticed that it isn't appropriate to
select a single Haar-like feature type for all output activations, since they
tend to behave similarly for most of the activations output by the
convolutional layer. In future work, it would be interesting to study the use
of hand-picked Haar features for each activation band, each one enhancing
different regions of interest present in our dataset.

\input{results/pooling/haar/figs}

Figure~\ref{fig:proj-haar} shows the PCA projections for all Haar feature types
we experimented with. Note how the plate samples are closely coupled with the
background ones, due to the simlarity between most of the activations output
by the Haar-like features.

\input{results/pooling/haar/tsne}

\section{Dataset Composition}\label{sec:dataset}

The dataset compositon step consists of extracting patches from the original
image, labeling them as either background or foreground and then
pseudo colorizing each patch based on an RGB color table.

\subsection{Patch Labeling}\label{sec:classification}

To label a patch as foreground or background, we used the plate annotations to
determine the percentage of the plate that was within each patch. Let
$t_{plate} \in \interval{0}{1}$ be the threshold ratio between plate pixels
within a patch and the total number of plate pixels in the image, a patch can
be classified as foreground if at least $t_{plate} \times \SI{100}{\percent}$
of the plate lies within it. In previous tasks, we used $t_{plate} =
\num{0.85}$.  However, with the full pipeline built, we found that the
classifier had a hard time distinguishing between plate patches and background
patches with a high percentage of the plate within them. To mitigate this
effect, we decreased $t_{plate}$ to \tp for all experiments, increasing the
number of patches labeled as plates and requiring a post-processing step to
select the best one.

\subsection{Patch Extraction}\label{sec:patch-extraction}

Each patch has dimensions $W_p \times H_p$ centered around position $(x, y)$ in
the original image. Using the plate annotations, it was possible to determine
the maximum width and height of a plate within the dataset's training
split. Let $r_{pad} \in [0, 1]$ be ratio of padding pixels applied to each
dimension of the patch relative to the maximum width and height of all patches,
we can define the patch dimensions as

\begin{align}
W_p &= \max_{w \in W} w \cdot r_{pad}, \\
H_p &= \max_{h \in H} h \cdot r_{pad}.
\end{align}

Accross the experiments, the ratio $r_{pad}$ was fixed to \rp, ensuring that it
would always be possible to find at least one patch annotated as a plate for
every image in the dataset. We tuned the patch dimensions to training split 1,
where we found that $W_p = \Wp$, $H_p = \Hp$. We used the same parameters for
all training splits when performing cross-validation.

Given the patch dimensions, we wish to determine the patch centers $(x, y)$ for
every image in the dataset. To do so, we implemented two methods: sliding window
approach outlined in Section~\ref{sec:sliding} and superpixel segmentation
detailed in Section~\ref{sec:postproc-sup}. The sliding window approach was used
throughout the experiments to find the best pipeline for patch classification.
We experimented with the superpixel approach only in the post-processing stage,
where the patch centers $(x, y)$ play a crucial role in selecting a patches
that have a large intersection with the plate pixels.

\subsubsection{Sliding Window}\label{sec:sliding}

The sliding window approach involves sliding a window of dimensions $W_p \times
H_p$ accross the original image and extracting a patch for every window that
doesn't extrapolate the image boundaries. To apply this strategy, we must define
the displacement $(D_x, D_y)$ between each patch. Let $r_{disp} \in [0, 1]$ be
the ratio between the patch displacements and the minimum width and height of
each patch, we can define the displacements as

\begin{align}
D_x &= \min_{w \in W} w \cdot r_{disp}, \\
D_y &= \min_{h \in H} h \cdot r_{disp}.
\end{align}

To determine the optimal displacement, we tuned $r_{disp}$ to select
approximately three plate patches per image to increase the chances of the
center one containing most of the plate inside. Let $p_{\min}$, $p_{\max}$, and
$p_{\mu}$, be the the minimum, maximum, and mean number of patches classified
as plates in each image, respectively.  We found that using $r_{disp} = \rd$
netted good results, where for training split 1, $p_{\min} = \num{2}$
(\SI{1}{\percent} of the time), $p_{\max} = \num{9}$ (\SI{1}{\percent} of the
time), and $p_{\mu} = \num{6.08}$. These results also translated to a good
performance in test split 1, where we found that $p_{\min} = \num{3}$
(\SI{1}{\percent} of the time) and $p_\mu = \num{6.39}$.

Figure~\ref{fig:patches} shows the result of the sliding window strategy on
twelve different patches for a sample image in the training split. Note how
there's an intersection between each patch due to the displacement being
smaller than the width and height of the patch. For this sample, four patches
were classified as plates.

\begin{figure}
    \centering
    \begin{subfigure}{0.24\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/patches/0_orig_0003_0021.png}
      \caption{Background}
      \label{fig:patch-21}
    \end{subfigure}
    \begin{subfigure}{0.24\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/patches/0_orig_0003_0022.png}
      \caption{Background}
      \label{fig:patch-22}
    \end{subfigure}
    \begin{subfigure}{0.24\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/patches/0_orig_0003_0023.png}
      \caption{Background}
      \label{fig:patch-23}
    \end{subfigure}
    \begin{subfigure}{0.24\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/patches/0_orig_0003_0024.png}
      \caption{Background}
      \label{fig:patch-24}
    \end{subfigure} \\
    \begin{subfigure}{0.24\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/patches/0_orig_0003_0026.png}
      \caption{Background}
      \label{fig:patch-26}
    \end{subfigure}
    \begin{subfigure}{0.24\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/patches/1_orig_0003_0027.png}
      \caption{Foreground}
      \label{fig:patch-27}
    \end{subfigure}
    \begin{subfigure}{0.24\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/patches/1_orig_0003_0028.png}
      \caption{Foreground}
      \label{fig:patch-28}
    \end{subfigure}
    \begin{subfigure}{0.24\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/patches/0_orig_0003_0029.png}
      \caption{Background}
      \label{fig:patch-29}
    \end{subfigure} \\
    \begin{subfigure}{0.24\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/patches/0_orig_0003_0031.png}
      \caption{Background}
      \label{fig:patch-31}
    \end{subfigure}
    \begin{subfigure}{0.24\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/patches/1_orig_0003_0032.png}
      \caption{Foreground}
      \label{fig:patch-32}
    \end{subfigure}
    \begin{subfigure}{0.24\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/patches/1_orig_0003_0033.png}
      \caption{Foreground}
      \label{fig:patch-33}
    \end{subfigure}
    \begin{subfigure}{0.24\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/patches/0_orig_0003_0034.png}
      \caption{Background}
      \label{fig:patch-34}
    \end{subfigure} \\
    \caption{Twelve patches extracted near a plate from a sample image.
    Patches~\subref{fig:patch-21}-\subref{fig:patch-26},
    \subref{fig:patch-29}-\subref{fig:patch-31}, and~\subref{fig:patch-34} are
    labeled as background. The remaining ones are labeled as foreground,
    since at least 50\% of the plate lies within the frame.}
    \label{fig:patches}
\end{figure}

\subsection{Colorization}

To pseudo colorize the images to RGB, matplotlib's~\cite{matplotlib}
\texttt{jet} colormap was used. The colormap maps values from 0 to 255 to the
RGB colorspace, from blue to red. A plot of the colormap used is given in
Figure~\ref{fig:cmap}. In Figure~\ref{fig:colorized}, we can see a sample
image after applying the \texttt{jet} colormap.

\begin{figure}
    \centering
    \includegraphics[width=0.55\textwidth]{./images/task-1/color/cmap_plot.pdf}
    \caption{Matplotlib's \texttt{jet} colormap with the $x$-axis representing
    the grayscale values and the $y$-axis the values for each of the
    channels in the RGB colorspace. The colorbar on the right shows the
    effective RGB color obtained for each of the grayscale values in the range
    [0, 255].}
    \label{fig:cmap}
\end{figure}

\begin{figure}
    \centering
    \begin{subfigure}{0.35\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/color/orig_0003.png}
      \caption{Original}
      \label{fig:color-orig}
    \end{subfigure}
    \hspace{1em}
    \begin{subfigure}{0.35\textwidth}
      \includegraphics[width=\textwidth]{./images/task-1/color/rgb_0003.png}
      \caption{Colorized}
      \label{fig:color-rgb}
    \end{subfigure}
    \caption{Sample application of matplotlib's \texttt{jet} colormap to the
    grayscale image~\subref{fig:color-orig} resulting in the colorized
    image~\subref{fig:color-rgb}.}
    \label{fig:colorized}
\end{figure}

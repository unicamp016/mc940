#!/bin/bash
#EXP_DIRS=( "kernel-norm/With-KN-3x3" "kernel-norm/With-KN-5x5" )
#ARGS=( "--batch_norm --kernel_norm --kernel_size 3" "--kernel_norm --batch_norm --kernel_size 5" )
#EXP_DIRS=( "pooling/Avg-3x3" "pooling/Avg-7x7" "pooling/Max-3x3", "pooling/Max-7x7", "pooling/Haar-3y-3x3" "pooling/Haar-3x-3x3" "pooling/Haar-plate-35x70" "pooling/Haar-plate-35x35" )
#ARGS=( "--batch_norm --kernel_norm --kernel_size 3 --pooling avgpool3x3" "--kernel_norm --batch_norm --kernel_size 3 --pooling avgpool7x7" "--batch_norm --kernel_norm --pooling maxpool3x3" "--batch_norm --kernel_norm --pooling maxpool7x7" "--batch_norm --kernel_norm --pooling haar3y3x3" "--batch_norm --kernel_norm --pooling haar3x3x3" "--batch_norm --kernel_norm --pooling haarplate35x70" "--batch_norm --kernel_norm --pooling haarplate35x35" )

#EXP_DIRS=( "pooling/Haar-3y-3x3" "pooling/Haar-3x-3x3" "pooling/Haar-plate-35x70" "pooling/Haar-plate-35x35" )
#ARGS=( "--batch_norm --kernel_norm --pooling haar3y3x3" "--batch_norm --kernel_norm --pooling haar3x3x3" "--batch_norm --kernel_norm --pooling haarplate35x70" "--batch_norm --kernel_norm --pooling haarplate35x35" )
EXP_DIRS=( "postproc/SP-MLP-FLIM-Max-7x7-V2" )
ARGS=( "--flim --pooling maxpool7x7 --classifier mlp --patch_strategy superpixel"  )
for i in "${!EXP_DIRS[@]}"; do
    python -m src.main -o bin/${EXP_DIRS[i]} ${ARGS[i]}
    mkdir -p report/images/${EXP_DIRS[i]}
    cp bin/${EXP_DIRS[i]}/train-1/pool/1_orig_0194_0032_*.png report/images/${EXP_DIRS[i]}/
    cp bin/${EXP_DIRS[i]}/split-*-pca.pdf report/images/${EXP_DIRS[i]}/
done

#python -m src.main -o bin/batch-norm/With-BN-5x5 --classifier mlp --kernel_size 5 --batch_norm
#python -m src.main -o bin/batch-norm/With-BN-3x3 --classifier mlp --kernel_size 3 --batch_norm

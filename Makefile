# Directories
SRC	= src
BIN = bin
ASSETS = assets
REPORT_DIR = report

# Files
REPORT = report
PROG = main
TASK_NAME = task-3

.PHONY: build run clean exp1 exp2 exp3 exp4 exp5 created_dirs

all: exp1 exp2 exp3 exp4 exp5

submit: build
	@cp $(BIN)/$(REPORT_DIR)/$(REPORT).pdf $(BIN)/$(TASK_NAME).pdf
	@mkdir -p $(BIN)/code
	@rsync -a Makefile $(ASSETS) $(SRC) $(REPORT_DIR) --exclude='**/__pycache__' $(BIN)/code
	@cd $(BIN); tar -jcvf $(TASK_NAME).tar.bz2 $(TASK_NAME).pdf code
	@rm -rf $(BIN)/code $(BIN)/$(TASK_NAME).pdf

exp1: $(SRC)/$(PROG).py | $(BIN)
	@python $< -a $(ASSETS) -o $(BIN)/$@ --overwrite \
		--patch_strategy sliding
	echo "Finished running $@"

exp2: $(SRC)/$(PROG).py | $(BIN)
	@python $< -a $(ASSETS) -o $(BIN)/$@ --overwrite \
		--batch_norm --patch_strategy sliding
	echo "Finished running $@"

exp3: $(SRC)/$(PROG).py | $(BIN)
	@python $< -a $(ASSETS) -o $(BIN)/$@ --overwrite \
		--kernel_norm --batch_norm --patch_strategy sliding
	echo "Finished running $@"

exp4: $(SRC)/$(PROG).py | $(BIN)
	@python $< -a $(ASSETS) -o $(BIN)/$@ --overwrite \
		--kernel_norm --patch_strategy sliding
	echo "Finished running $@"

exp5: $(SRC)/$(PROG).py | $(BIN)
	@python $< -a $(ASSETS) -o $(BIN)/$@ --overwrite \
		--kernel_norm --batch_norm --patch_strategy superpixel
	echo "Finished running $@"

build: $(BIN)/$(REPORT_DIR)/$(REPORT).pdf

clean:
	@find . -name "*.pyc" -delete
	@rm -rf $(BIN)/*

write-report: $(REPORT_DIR)/$(REPORT).tex | create_dirs
	latexmk -pdf -pvc -cd $< \
		-aux-directory=$(BIN)/$(REPORT_DIR) \
		-output-directory=$(BIN)/$(REPORT_DIR)

$(BIN)/$(REPORT_DIR)/$(REPORT).pdf: $(REPORT_DIR)/$(REPORT).tex | create_dirs
	latexmk -pdf -cd $< \
		-aux-directory=$(BIN)/$(REPORT_DIR) \
		-output-directory=$(BIN)/$(REPORT_DIR)

create_dirs:
	@for OUTPUT in $(shell cd $(REPORT_DIR); find . -type d); do \
	  mkdir -p $(BIN)/$(REPORT_DIR)/$$OUTPUT; \
	done

$(BIN):
	@mkdir -p $(BIN)
